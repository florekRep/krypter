\chapter{Metody zabezpieczania kodu}
\label{cha:securing_techniques}

Obfuskacja – czyli proces zaciemniania kodu, polega na przekształcaniu programu w taki sposób aby zachować jego oryginalną funkcjonalność przy jednoczesnym utrudnieniu jej zrozumienia na podstawie analizy tego programu. Obfuskacja przeprowadzona może być na kodzie źródłowym, pośrednim lub maszynowym. Najefektywniejsza jednak jest ona na poziomie po kompilacji. Dzieje się tak ponieważ kompilator mógłby, w celu poprawienia wydajności, usunąć niektóre transformacje, neutralizując tym samym obfuskacje.
\\Możliwe jest wydzielenie trzech rodzajów transformacji \cite{ObfuscationTransformations}:
\begin{itemize}[noitemsep]
\item Layout transformation – zmianie podlegają nazwy zmiennych, formatowanie, komentarze
\item Data transformation – zmieniane są dane, czas życia zmiennych, łączenie tablic, konwersja statycznych danych do procedur
\item Code transformation – zmieniany jest przebieg instrukcji programu, wykonywane jest rozwijanie pętli, zmiana warunków w pętlach i blokach warunkowych, zmieniana jest kolejności instrukcji.
\end{itemize}

W kolejnych rozdziałach zostaną omówione popularne techniki obfuskacji i ochrony przed niepożądanym dostępem do kodu programu. Rozwiązania te są szczególnie przydatne dla programów napisanych w Javie lub językach .Net, gdyż bez odpowiedniego zabezpieczenia, ich dekompilacja jest zadaniem prostym. Obfuskacja pozwala na ochronę własności intelektualnej, ukrywa luki bezpieczeństwa, ukrywa mechanizm zabezpieczeń licencyjnych (chroni przed kopiowaniem i tak zwanym crackowaniem programów). Część z prezentowanych technik może również prowadzić do zmniejszenia rozmiaru wynikowej aplikacji.
Do wad obfuskacji należą problemy z przenośnością, zarówno między wersjami jak również różnymi maszynami wirtualnymi (Mono, CLR). Z powodu dużych zmian w strukturze programu jego debugowanie jest znacznie utrudnione lub też niemożliwe. Refleksja, czyli metoda na odnajdywanie informacji o typach w trakcie działania programu, również może nie działać ponieważ typy te mogły zostać zmienione w wyniku obfuskacji. Warto także zdawać sobie sprawę, że samo zastosowanie obfuskacji nie powstrzyma odpowiednio zdeterminowaną osobę (lub zespół) przed analizą programu, może jednak to zadanie w znacznym stopniu utrudnić.


\section{Szyfrowanie łańcuchów znaków}
\label{sec:string_enc_desc}

Metoda ta jest powszechnie stosowana w niemal wszystkich komercyjnych protectorach. Jej działanie polega na ukryciu wykorzystywanych w programie łańcuchów znaków. Zazwyczaj łańcuchy zawarte w kodzie niosą ze sobą wiele informacji na temat działania programu. Dla przykładu koniecznym może być zabezpieczenie tak zwanego connection stringa – czyli ciąu zawierającego parametry do połączenia z zewnętrzną bazą danych. Również wszystkie zapisane w postaci łańcuchów hasła lub komendy które nie powinny być w prosty sposób dostępne dla użytkowników powinny być zabezpieczone. Ponieważ w pewnym momencie działania programu wartości te muszą być dostępne w swojej oryginalnej postaci, nie jest możliwe ich całkowite zaszyfrowanie, raczej stosowane jest kodowanie które może być odwrócone w trakcie działania (np. AES ze stałym i zapisanym w kodzie kluczem) \cite{AndroidAntiMalware}.
\\Zabezpieczenie to działa według następującego schematu \cite{Hartung}:
\begin{enumerate}[noitemsep]
\item We wszystkich metodach wyszukiwane są instrukcje \texttt{ldstr}
\item łańcuch znakowy będący argumentem ldstr jest szyfrowany
\item W miejsce instrukcji ldstr umieszczany jest ciąg instrukcji wywołujących funkcje deszyfrującą wraz z właściwymi parametrami
\end{enumerate}

Zaszyfrowany ciąg może być przechowywany w miejscu oryginalnego w strumieniu \#US pod warunkiem że nie zmieniła się jego długość. Może również być przechowywany w strumieniu \#Blob lub jako zasób.
\\Aby poprawić wydajność tej metody przy dużej liczbie odwołań do ciągów tekstowych, ich dekodowanie odbywa się tylko raz, a kolejne wywołania funkcji deszyfrującej zwracają wartość z kolecji podręcznej.
Dla przykładu w protectorze CryptoObfuscator ciągi znaków w postaci zaszyfrowanej przechowywane są jako zasób. Wszystkie instrukcje ldstr zamieniane są na wywołanie funkcji z parametrem int identyfikującym wymagany ciąg. Na początku działania programu zasób jest ładowany i deszyfrowany do tablicy w pamięci, odbywa się to w konstruktorze statycznym (metoda \texttt{<Module>.cctor}). Następnie każde odwołanie do łańcucha odbywa się przez metodę która zwraca go z wcześniej utworzonej tablicy. Rozwiązanie to nie wprowadza narzutu w postaci każdorazowego deszyfrowana.\\
Funkcjonalność ta może być rozszerzona do szyfrowania pozostałych stałych: liczbowych, tablic bajtów itd. Rozwiązanie takie zastosowane jest w ConfuserEx \cite{DissectingConfuserConstatns}.


\section{Zmiana widocznych w programie nazw}
\label{sec:rename_desc}

Wszystkie nazwy klas oraz metod publicznych dostępne są w tabeli z metadanymi, ich odzyskanie jest więc proste. Zdradzają one dużo informacji na temat działania poszczególnych komponentów jak i całej aplikacji. Poprzez zaciemnienie nazw można utrudnić analizowanie kodu powstałego po dekompilacji \cite{JavaRenaming}. Nazwy zazwyczaj są zmieniane wykorzystując jedną ze strategii:
\begin{itemize}[noitemsep]
\item Krótkie nazwy – wykorzystywane są kolejne znaki alfabetu zaczynając od ‘a’, wykorzystywany jest również fakt, że różne elementy mogą posiadać tę samą nazwę jeżeli różnią się typem. W ten sposób w klasie może znajdować się wiele funkcji o nazwie \texttt{a} jeżeli mają różne deklaracje. Podobnie mogą istnieć pola \texttt{int a}, \texttt{byte[] a} które mimo tej samej nazwy są rozróżnialne przez CLR. Kod po dekompilacji jest wówczas bardzo nieczytelny ponieważ nazwa zmiennej może odwoływać się do różnych elementów. Takie nazewnictwo jest niedozwolone m.in. w języku C\#, ale nie w CLR.
\item Hashe z nazw – wszystkie nazwy zastępowane są swoimi hashami. Zazwyczaj nazwy są dosyć długie co prowadzi do mniej czytelnego kodu.
\item Fałszywe nazwy – zastąpienie nazw oryginalnych nazwami które nic nie mówią o opisywanym elemencie ale są poprawnymi i zrozumiałymi ciągami znaków.
\item Znaki specjalne – wykorzystywane są tutaj znaki z tablicy Unicode które należą do egzotycznych alfabetów (chiński, arabski) lub są niedrukowalne. Dodatkowym atutem tej metody jest to, że może powodować nieprzewidywane zachowanie w programach dekompilujących które w niepoprawny sposób obsługują unicode.
\end{itemize}
Metoda ta jest powszechnie wykorzystywana we wszystkich protektorach. 
Posiada jednaka szereg wad. Wszystkie moduły korzystające z zabezpieczonej biblioteki muszą posiadać informacje o nazwach po obfuskacji aby poprawnie z niej korzystać, inaczej nie będzie możliwe załadowanie biblioteki i odnalezienie wymaganego elementu. Dlatego też bardzo często program w ten sposób zabezpieczony nie może współdziałać z żadnym innym programem na zasadzie odwoływania się do jego funkcji. Wszystkie protectory umożliwiają wiec załadowanie szeregu powiązanych assembly, tak aby zmiany wprowadzane w nazewnictwie zmiennych i metod mogły zostać rozpropagowane do wszystkich wykorzystujących je komponentów. Jest to jednak rozwiązanie nierozszerzalne.
\\Kolejnym problemem jest debugowanie aplikacji po obfuskacji, ponieważ nazwy są zmienione środowisko programistyczne nie jest w stanie dostarczyć normalnych narzędzi wspomagających analizę działania programu. Również generowane informacje o wyjątkach, jak stack trace są zaciemnione przez nowe nazwy. Aby rozwiązać ten problem często generowane są pliki zawierające informację o odwzorowaniu pomiędzy oryginalną a zamienioną nazwą. Takie pliki mappingu mogą następnie być wykorzystane do odszyfrowania stack trace, bądź wspomagać analizę crash dumpów. 
\\W wypadku wykorzystywania przez program refleksji mogą pojawić się problemy ponieważ nazwy zapisane w kodzie mogą nie zostać odpowiednio poprawione. Między innymi z tego powodu niezalecane jest obfuskowanie aplikacji ASP.NET


\section{Pośrednia warstwa szyfrująca}
\label{sec:kryptik_desc}

Oryginalny plik wykonywalny poddawany jest kompresji. Następnie specjalnie przygotowany program w trakcie działania dekompresuje go i rozpoczyna jego wykonanie. Program dekompresujący nazywany jest stubem. Oba te programy mogą być napisane w różnych technologiach, np. stub jako aplikacja natywna a plik kompresowany to program zarządzany. Najprościej jest jednak wykorzystać to samo środowisko. Dla aplikacji .Net wystarczy wywołać funkcję \texttt{System.Assembly.Load}, która poprawnie załaduje program \cite{NetMalware}.
\\Problematycznym jest ładowanie aplikacji zarządzanych z poziomu kodu natywnego. Trudności wynikają z faktu że przy normalnym uruchomieniu programu .Net loader systemowy wykrywa, że jest to aplikacja zarządzana i sam ładuje środowisko CLR wraz z odpowiednimi zależnościami. W przypadku aplikacji natywnych CLR nie jest ładowany. Musi więc być załadowany ręcznie poprzez wykorzystanie interfejsu COM.

Prosta koncepcja zabezpieczenia nie oznacza jednak że jest ono nieskuteczne. Warstwę szyfrującą można bowiem dowolnie komplikować, utrudniając rozpakowanie programu właściwego. Mogą się tam znaleźć instrukcje sprawdzające spójność programu, czy nie został on zmodyfikowany. Techniki anti-debug również utrudnią analizę protectora. Dodatkowo zastosowana metoda kompresji / szyfrowania także stanowić będzie kolejną warstwę zabezpieczeń.
\\Zastosowanie warstwy natywnej uniemożliwi dekompilację kodu i zmusi potencjalnego atakującego do analizy natywnego stuba. Niewątpliwą wadą tego rozwiązania jest to że w pewnym momencie działania w pamięci znajdować będzie się pełna wersja chronionego pliku, która następnie może zostać zapisana na dysku. Można bronić się przed takimi atakami monitorując czy proces nie jest pod kontrolą debuggera, oraz uniemożliwiać czytanie pamięci procesu zewnętrznym programom.
W przypadku gdy warstwa szyfrująca jest aplikacją .Net, uniwersalną metodą jej odpakowania jest ustawienie breakpointa (pułapki debugera) na metodą \texttt{Load}, a następnie wykonanie zrzutu pamięci procesu i wyekstraktowanie właściwego pliku wykonywalnego. Najprościej taką czynność zautomatyzować za pomocą skryptu do WinDbg \cite{Hartung}.
\begin{lstlisting}[caption={Skrypt do zrzucania pamięci programu ładowanego dynamicznie}]
sxe ld clr; g
.loadby sos clr
bp clr!AssemblyNative::LoadFromBuffer
"dd (edx - 4) L1; da edx"
\end{lstlisting}
W przypadku wersji natywnej należy prześledzić wykorzystanie obiektów COM w celu znalezienia odwołania do funkcji \texttt{ExecuteInDefaultAppDomain} lub podobnej. Następnie ustawić breakpoint na jej wywołaniu i wykonać zrzut pamięci. Ponieważ metody te wymagają działania programu pod kontrolą debugera należy pamiętać o różnych metodach anty-debug, które mogą uniemożliwić jego działanie.


\section{Szyfrowanie metod}
\label{sec:method_enc_desc}

Najwięcej informacji o programie zawartych jest w jego metodach. Dlatego ich skuteczna ochrona utrudni proces reverse engineeringu. Szyfrowanie metod polega na zakodowaniu ciała każdej metody tak aby nie można było dokonać jej statycznej analizy. Po otwarciu w dekompilatorze tak zabezpieczonego programu żadna z metod nie będzie wyświetlana w czytelnej postaci. Aby jednak zapewnić poprawne działanie takiej aplikacji konieczne jest odkodowanie jej funkcji. Wśród protektorów spopularyzowane zostały dwie techniki. Pierwsza zakłada jednokrotne odszyfrowanie wszystkich funkcji na początku działania programu. Funkcja deszyfrująca zazwyczaj znajduje się w konstruktorze statycznym (\texttt{.cctor}) klasy zawierającej Entry Point lub w \texttt{<Module>.cctor()}, konstruktorze statycznym znajdującym się w specjalnym nie widocznym module. Jest on uruchamiany na samym początku działania programu. Podobnie konstruktory statyczne uruchamiane są zanim zostanie wykonana jakakolwiek funkcja z danej klasy. Technika ta jest podobna do wykorzystania TLS w protectorach natywnych. Wszystkie funkcje są już odszyfrowane gdy uruchamiana jest kompilacja JIT \cite{DissectingConfuserAntiTamper}. 

Druga z technik wykorzystuje informacje o szczegółach implementacji CLR. Podczas kompilacji JIT każdej z metod wywoływana jest funkcja \texttt{int CompileMethod(IntPtr thisPtr, [In] IntPtr corJitInfo, [In] CorMethodInfo* methodInfo, CorJitFlag flags, [Out] IntPtr nativeEntry, [Out] IntPtr nativeSizeOfCode)}. Jednym z jej parametrów jest struktura \texttt{CorMethodInfo} \ref{lst:CorMethodInfo}, która dostarcza informacji o kompilowanej metodzie, między innymi zawiera tablicę bajtów z kodem CIL, jej rozmiar, informacje o argumentach i zmiennych lokalnych. Działanie tej techniki opiera się na podmianie oryginalnej funkcji \texttt{CompileMethod} na taką która najpierw odszyfruje ciąg bajtów a następnie wywoła wersje oryginalną aby poprawnie zakończyć kompilację \cite{ClrInjection}.
\begin{lstlisting}[caption={Uproszczona definicja struktury CorMethodInfo}, label={lst:CorMethodInfo}]
struct CorMethodInfo {
  IntPtr methodHandle;
  IntPtr moduleHandle;
  IntPtr ilCode;
  UInt32 ilCodeSize;
  UInt16 maxStack;
  UInt16 EHCount;
  UInt32 corInfoOptions;
}
\end{lstlisting}
Istotnym problemem jest tutaj zainstalowanie zmienionej funkcji CompileMethod. Ponieważ jest to funkcja wewnętrzna frameworku, może ulegać zmianie wraz z nowymi wersjami. Z tego względu metoda ta jest nieprzenośna i może działać niestabilnie. Przy jej implementacji konieczne jest wykorzystanie kodu natywnego do instalacji hooka, lub skorzystanie z dyrektywy \texttt{unsafe} w kodzie C\#. Do odkodowania metody dochodzi podczas jej kompilacji JIT. Jeśli więc metoda nigdy nie zostanie wywołana to również nie dojdzie do jej odkodowania.

Podczas analizy statycznej tak zabezpieczonych programów jedynym widocznym elementem jest funkcja deszyfrująca. Odpakowanie tego zabezpieczenia jest problematyczne ponieważ nie ma możliwości wykonania prostego zrzutu pamięci, gdyż na żadnym etapie nie ma go w pamięci. Najprostszą metodą jest więc przygotowanie statycznego unpackera, co jednak wiąże się z koniecznością dokładnej analizy algorytmu kodowania. Rozwiązanie bardziej generyczne może opierać się na zainstalowaniu hooka na metodzie CompileMethod lub wymuszeniu uruchamiania konstruktorów statycznych \cite{NetMalwareTricks}. Następnie zaś na podstawie wyniku działania tych funkcji podjąć próbę rekonstrukcji oryginalnego programu. Rozwiązanie takie zaimplementowane jest w \texttt{de4dot} (\texttt{DynamicMethodDecoder}).


\section{Zmiana przepływu kodu}
\label{sec:control_flow_desc}

Transformacje typu code flow obfuscation, należą do jednych z najbardziej zaawansowanych technik obfuskacji. Wiele artykułów naukowych poświęconych jest tworzeniu nowych i analizie już istniejących metod, m.in. \cite{ControlFlowBasedObfuscation} \cite{JavaCFMethods}. Wśród przedstawianych można wyróżnić następujące kategorie transformacji \cite{JavaCFObfuscation}:
\begin{itemize}[noitemsep]
\item Wstawianie martwego kodu (dead code injection), w ten sposób wprowadzany jest kod który nigdy nie zostanie wykonany. Może to być zrealizowane za pomocą instrukcji warunkowej o znanym z góry wyniku działania (opaque predicate). Podczas statycznej analizy nie zawsze możliwe jest określenie ścieżki która będzie wykonana, więc obie traktowane są jako poprawne gałęzie programu. W ten sposób można uniemożliwić działanie dekompilatorom oraz zaciemnić przepływ kodu.
\item Tworzenie dodatkowych funkcji. Główny fragment kodu dzielony jest na pod-fragmenty które następnie umieszczane są w osobnych metodach, tracona jest więc spójność kodu, który odpowiada za daną funkcjonalność.
\item Zrównoleglenie kodu. Wymaga dokładnej analizy zakresu żywotności zmiennych oraz ich wykorzystania przez bloki podstawowe, następnie można podzielić kod na fragmenty które będą wykonywane równolegle. Z powodu dużego narzutu zarówno przy transformacji jak i w czasie wykonania technika nie jest powszechnie wykorzystywana.
\item Zmiana warunku pętli, poprzez skomplikowanie go. Wynik działania będzie taki sam, ale ciąg instrukcji obliczających warunek będzie bardziej skomplikowany. Po raz kolejny zastosowanie znajdują tutaj opaque predicates.
\item Dodawanie instrukcji które nie mają wpływu na działanie programu, ale generują dodatkowy kod utrudniający analizę. Możliwe jest wykorzystanie operacji nop, jednak jest ona trywialna do usunięcia. Znacznie lepsze efekty uzyskuje się przy użyciu instrukcji które niwelują efekty swojego działania. Np. inkrementowanie i dekrementowanie tej samej zmiennej.
\item Rozwijanie pętli oraz usuwanie wywołań funkcji zastępując je ciałem funkcji (inlining), to techniki optymalizacji kodu często używane przez kompilatory. Ich skutkiem ubocznym jest generowanie mniej czytelnego kodu.
\item Spłaszczanie kodu i zmiana jego rozkładu w funkcji (control flow flattening) \cite{ControlFlowFlattening}. Bardzo popularna technika, szeroko wykorzystywana przez komercyjne protectory. 
Kod metody najpierw rozkładany jest na bloki podstawowe (basic block), następnie ich kolejność w kodzie jest zmieniana. Aby zachować oryginalną kolejność wykonania wprowadzana jest instrukcja switch która będzie sterowała kolejnością uruchamiania tych bloków. Kontrolowana jest zmienną typu int, jest ona aktualizowana na końcu każdego bloku, tak aby wskazać następny do wykonania. Wszystkie pętle, bloki warunkowe oraz bloki zagnieżdżone są usuwane i syntezowane za pomocą instrukcji switch. Dzięki temu tracony jest pierwotny wygląd przepływu kodu.
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{img/cf_flattening}
\caption{Efekt działania obfuskacji typu control flow flattening}
\label{fig:cf_flattening}
\end{figure}

\section{Przekierowanie metod}
\label{sec:method_proxy_desc}

Protectory natywne bardzo często uszkadzały tablicę importów oryginalnego programu. Działanie to polegało na utworzeniu stubu do funkcji bibliotecznej, który w najprostszej wersji wykorzystywał JMP aby przenieść wykonanie do właściwej funkcji. W wersjach rozbudowanych kopiowany było kilka początkowych bajtów funkcji właściwej a skok wykonywany był nie do początku ale pod dalszy offset w funkcji. Technika ta nazywana była kradzieżą importów (import stealing) \cite{AntiDumping}. 
\\Podobne rozwiązanie może zostać zaimplementowane na platformie .Net. Tutaj jednak nie ma dostępu do tablicy importowanych funkcji. Używane jest więc pośrednie rozwiązanie. Dla każdej wykorzystywanej przez program funkcji zewnętrznej tworzona jest specjalna funkcja w programie, która sygnaturą odpowiada tej bibliotecznej. Jej jedynym zadaniem jest wywołanie swojego odpowiednika z biblioteki. Do generowania takich funkcji mogą zostać wykorzystane typy generyczne i delegaty. Następnie wszystkie wywołania funkcji bibliotecznych zamieniane są na wywołania wcześniej wygenerowanych metod. W ten sposób utracona zostaje informacja o metodach zewnętrznych wykorzystywanych przez daną funkcję. Zaletą tego rozwiązania jest również fakt, że nie powoduje narzutu w czasie wykonania, ponieważ JIT zazwyczaj usunie wywołanie pośrednie, generując optymalny kod.


\section{Anti Debug / Disassembly / Tampering}
\label{sec:anti_desc}

Poza głównymi metodami zabezpieczeń istnieje szereg rozwiązań, których głównym zadaniem jest utrudnienie analizy programu. Zazwyczaj stosowane są w połączeniu z innymi zaprezentowanymi technikami.

Anti Debug, polega na sprawdzaniu czy proces programu nie jest uruchomiony pod kontrolą debugera. Jeśli dubuger zostanie wykryty działanie program jest przerywane. Możliwe metody rozpoznawania debugera \cite{AntiUnpack}:
\begin{itemize}[noitemsep]
\item Przeszukiwanie otwartych okien pod kątem znanych nazw debugerów
\item Funkcja \texttt{kernel32!IsDebuggerPresent}
\item Metoda \texttt{Debugger.IsAttached}
\item Sprawdzenie pola \texttt{NtGlobalFlags} w strukturze procesu PEB
\end{itemize}
Typowa implementacja polega na uruchomieniu przy starcie programu wątku, który będzie periodycznie sprawdzał czy debuger jest podłączony, najlepiej wykorzystując więcej niż jedną metodę jego detekcji. Dzięki temu możliwe będzie wykrycie debugera który został podłączony w trakcie działania programu.

Anti Disassembly ma za zadanie utrudnić disasemblacje oraz dekompilację kodu. Najprostsze rozwiązanie polega na dodaniu do assembly atrybutu \texttt{SuppressIldasm}. ConfuserEx wykorzystuje nieudokumentowane pola w metadanych aby spowodować zawieszenie dekompilatorów  \cite{DissectingInvMetadata}. Innym popularnym rozwiązaniem jest wstawienie na początku metody ciągu instrukcji CIL, które nie spowodują funkcjonalnej zamiany w kodzie, jednocześnie powodując błąd dekompilacji. 
Na początku znajduję się skok do właściwej części metody który zawsze będzie wykonany. Zaraz za skokiem jest ciąg instrukcji które powodują crash dekompilatora. Nie są one jednak nigdy wykonywane.

Anti Tempering, chroni assembly przed modyfikacją, jeżeli zostanie ona wykryta, program kończy działanie. Najprostsza implementacja opiera się na policzeniu funkcji skrótu (hash) z fragmentu programu w pamięci i porównaniu go z wcześniej wyliczoną wartością.


\section{Wirtualizacja}
\label{sec:virtualization_desc}

Narzędzia typu VMProtect oraz Themida wykorzystują wirtualizacje do zabezpieczania aplikacji natywnych. Ich działanie polega na transformacji kodu maszynowego do specjalnego bajtkodu, który następnie interpretowany jest przez maszynę wirtualną. Często zarówno maszyna jak i bajtkod są unikalnie generowane przy każdym zabezpieczaniu. Jest to obecnie najlepsze istniejące zabezpieczenie, ponieważ bezpowrotnie tracona jest informacja o kodzie pierwotnym. Transformacja jest jednokierunkowa. To sprawia że analiza tak zabezpieczonych programów jest zadaniem bardzo trudnym, a tworzenie narzędzi do generycznego odpakowywania wręcz niemożliwa. Dla platformy .Net również istnieją protectory wspierające wirtualizację m.in. Agile.NET Eazfuscator.NET, wówczas maszyna wirtualna protectora działa ponad maszyną CLR.


\section{Szyfrowanie zasobów}
\label{sec:res_enc_desc}

Jeżeli podczas ładowania zasobu wystąpi błąd, np. zasób nie mógł zostać zlokalizowany, wywoływane jest zdarzenie AppDomain.ResourceResolve. Dostarczając odpowiednią funkcję (delegatę) można w trakcie działania odszyfrować i dostarczyć zasób. Kod programu nie musi ulegać zmianie, co jest atutem tego rozwiązania \cite{NetManifest}.

\subsubsection*{}
Zaprezentowane w tym rozdziale techniki obfuskacji są wynikiem zebrania różnych pomysłów prezentowanych w artykułach, wpisach na blogach oraz oprogramowaniu dostępnym na rynku. Większość dostępnych systemów zabezpieczania kodu umożliwia przeprowadzenie omówionych w tym rozdziale transformacji. Różnice w nich występujące odnoszą się do szczegółów implementacji i nie wpływają na uogólniony koncept. Bardzo dobrym źródłem informacji o tych metodach są protectory, zarówno open-source \cite{confuser} jak również komercyjne. Prostą metodą badawczą można analizować poszczególne zabezpieczenia. Wykorzystując program testowy możliwie prosty, ale posiadający elementy podlegające zabezpieczaniu można aplikować pojedyncze transformacje w poszczególnych protectorach i analizować program wynikowy. Duża liczba systemów umożliwia ustawienie opcji obfuskacji, w szczególności wybranie tylko jednej z nich. W ten izolowany sposób można analizować działanie poszczególnych komponentów.
\\Zrozumienie mechanizmów działania zabezpieczeń w połączeniu z wiedzą o platformie .Net, pozwoli w kolejnym rozdziale na implementację zaprezentowanych w tym rozdziale technik. 

