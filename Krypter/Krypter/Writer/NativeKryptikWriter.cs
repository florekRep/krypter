﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using dnlib.DotNet;
using dnlib.DotNet.Writer;
using Krypter.Utils;

namespace Krypter.Writer
{
    public class NativeKryptikWriter : IProtectedModuleWriter
    {
        private const uint RT_RCDATA = 10;
        private readonly string stubPath;
        private readonly ModuleWriterListener writerListener;

        public NativeKryptikWriter(ModuleWriterListener writerListener, string stubPath)
        {
            this.writerListener = writerListener;
            this.stubPath = stubPath;
        }

        public void Write(ModuleDef module, string filename)
        {
            var key = RandomGenerator.GenerateKey();
            var programData = EncryptData(WriteOriginalModule(module), key);
            DropStub(filename);
            var dataHandle = GCHandle.Alloc(programData, GCHandleType.Pinned);
            var keyHandle = GCHandle.Alloc(BitConverter.GetBytes(key), GCHandleType.Pinned);
            var handle = BeginUpdateResource(filename, true);
            UpdateResource(handle, RT_RCDATA, "code", 0, dataHandle.AddrOfPinnedObject(),
                Convert.ToUInt32(programData.Length));
            UpdateResource(handle, RT_RCDATA, "key", 0, keyHandle.AddrOfPinnedObject(), 4);
            EndUpdateResource(handle, false);
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr BeginUpdateResource(string pFileName,
            [MarshalAs(UnmanagedType.Bool)] bool bDeleteExistingResources);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool UpdateResource(IntPtr hUpdate, uint lpType, string lpName, ushort wLanguage,
            IntPtr lpData, uint cbData);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool EndUpdateResource(IntPtr hUpdate, bool fDiscard);

        private void DropStub(string filename)
        {
            File.Copy(stubPath, filename, true);
        }

        private byte[] EncryptData(byte[] data, uint key)
        {
            var decryptionKeyBytes = BitConverter.GetBytes(key);
            Trace.Write(string.Format("k: {0:X} {1:X} {2:X} {3:X}", decryptionKeyBytes[0], decryptionKeyBytes[1],
                decryptionKeyBytes[2], decryptionKeyBytes[3]));
            for (var i = 0; i < data.Length; i++)
            {
                data[i] = (byte) (data[i] ^ decryptionKeyBytes[i%decryptionKeyBytes.Length]);
            }
            return data;
        }

        private byte[] WriteOriginalModule(ModuleDef module)
        {
            var options = new ModuleWriterOptions(module, writerListener);
            var moduleWriter = new ModuleWriter(module, options);
            byte[] data;
            using (var memoryStream = new MemoryStream())
            {
                moduleWriter.Write(memoryStream);
                data = memoryStream.ToArray();
            }
            writerListener.Reset();
            return data;
        }
    }
}