﻿using dnlib.DotNet;
using dnlib.DotNet.Writer;

namespace Krypter.Writer
{
    public class ListenerModuleWriter : IProtectedModuleWriter
    {
        private readonly ModuleWriterListener writerListener;

        public ListenerModuleWriter(ModuleWriterListener writerListener)
        {
            this.writerListener = writerListener;
        }

        public void Write(ModuleDef module, string filename)
        {
            var options = new ModuleWriterOptions(module, writerListener);
            
            var moduleWriter = new ModuleWriter(module, options);
            
            moduleWriter.Write(filename);
        }
    }
}