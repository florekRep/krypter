﻿using dnlib.DotNet;

namespace Krypter.Writer
{
    public interface IProtectedModuleWriter
    {
        void Write(ModuleDef module, string filename);
    }
}