﻿using dnlib.DotNet;

namespace Krypter.Writer
{
    public class NetKryptikWriter : IProtectedModuleWriter
    {
        public void Write(ModuleDef module, string filename)
        {
            module.Write(filename);
        }
    }
}