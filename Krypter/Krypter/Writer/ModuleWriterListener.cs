﻿using System;
using dnlib.DotNet.Writer;

namespace Krypter.Writer
{
    public class ModuleWriterListener : IModuleWriterListener
    {
        void IModuleWriterListener.OnWriterEvent(ModuleWriterBase writer, ModuleWriterEvent evt)
        {
            OnWriteEvent?.Invoke(writer, evt);
        }

        public void Reset()
        {
            OnWriteEvent = null;
        }

        public event EventHandler<ModuleWriterEvent> OnWriteEvent;
    }
}