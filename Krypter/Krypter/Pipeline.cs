﻿using System;
using System.Collections.Generic;
using dnlib.DotNet;
using Krypter.Protections;

namespace Krypter
{
    public class Pipeline
    {
        private readonly IList<IProtectionStage> stages = new List<IProtectionStage>();

        public ModuleDef Run(ModuleDef module)
        {
            var protectedModule = module;
            foreach (var protection in stages)
            {
                Console.WriteLine("\t[*] {0}", protection.ActionName);
                protectedModule = protection.Execute(protectedModule);
            }
            return protectedModule;
        }

        public void AddStage(IProtectionStage stage)
        {
            stages.Add(stage);
        }
    }
}