﻿using System;
using System.Collections.Generic;
using System.Linq;
using dnlib.DotNet;
using dnlib.DotNet.Emit;

namespace Krypter
{
    /* Based on ConfuserEx InjectionHelper */

    public class RuntimeInjector
    {
        private readonly Dictionary<IDnlibDef, IDnlibDef> mapping = new Dictionary<IDnlibDef, IDnlibDef>();
        private readonly ModuleDef targetModule;
        private Importer importer;

        public RuntimeInjector(ModuleDef targetModule)
        {
            this.targetModule = targetModule;
            importer = new Importer(targetModule, ImporterOptions.TryToUseTypeDefs);
        }

        public static TypeDef Inject(TypeDef sourceType, TypeDef targetType)
        {
            return new RuntimeInjector(targetType.Module).InjectImpl(sourceType, targetType);
        }

        public static MethodDef Inject(MethodDef sourceMethod, MethodDef targetMethod, int codeOffset)
        {
            if (sourceMethod.Body.ExceptionHandlers.Count > 0)
                throw new ArgumentException("source method cannot contain exception handling code");

            //clear ret (nasty!)
            foreach (
                var instruction in
                    sourceMethod.Body.Instructions.Where(instruction => instruction.OpCode == OpCodes.Ret).Reverse())
            {
                sourceMethod.Body.Instructions.Remove(instruction);
            }
            new RuntimeInjector(targetMethod.Module).MapMethodCode(sourceMethod, targetMethod, codeOffset);
            targetMethod.Body.SimplifyBranches();
            targetMethod.Body.SimplifyMacros(targetMethod.Parameters);
            return targetMethod;
        }

        private TypeDef InjectImpl(TypeDef sourceType, TypeDef targetType)
        {
            mapping[sourceType] = targetType;
            CloneType(sourceType);
            CopyType(sourceType);
            return targetType;
        }

        private TypeDef CloneType(TypeDef typeDef)
        {
            if (!mapping.ContainsKey(typeDef))
            {
                mapping[typeDef] = Clone(typeDef);
            }
            var clonnedType = (TypeDef) mapping[typeDef];

            foreach (var nestedType in typeDef.NestedTypes)
            {
                clonnedType.NestedTypes.Add(CloneType(nestedType));
            }
            foreach (var method in typeDef.Methods)
            {
                var clonnedMethod = Clone(method);
                clonnedType.Methods.Add(clonnedMethod);
                mapping[method] = clonnedMethod;
            }
            foreach (var field in typeDef.Fields)
            {
                var clonnedField = Clone(field);
                clonnedType.Fields.Add(clonnedField);
                mapping[field] = clonnedField;
            }
            return clonnedType;
        }

        private void CopyType(TypeDef typeDef)
        {
            Copy(typeDef);
            foreach (var nestedType in typeDef.NestedTypes)
            {
                CopyType(nestedType);
            }
            foreach (var methodDef in typeDef.Methods)
            {
                Copy(methodDef);
            }
            foreach (var fieldDef in typeDef.Fields)
            {
                Copy(fieldDef);
            }
        }

        private void Copy(MethodDef methodDef)
        {
            var mappedMethodDef = (MethodDef) mapping[methodDef];
            mappedMethodDef.Signature = importer.Import(methodDef.Signature);
            mappedMethodDef.Parameters.UpdateParameterTypes();

            if (methodDef.ImplMap != null)
            {
                mappedMethodDef.ImplMap = new ImplMapUser(
                    new ModuleRefUser(targetModule, methodDef.ImplMap.Module.Name), methodDef.ImplMap.Name,
                    methodDef.ImplMap.Attributes);
            }

            foreach (var customAttribute in methodDef.CustomAttributes)
            {
                mappedMethodDef.CustomAttributes.Add(
                    new CustomAttribute((ICustomAttributeType) TryResolveImport(customAttribute.Constructor) ??
                                        (ICustomAttributeType) importer.Import(customAttribute.Constructor)));
            }

            if (methodDef.HasBody)
            {
                mappedMethodDef.Body = new CilBody(methodDef.Body.InitLocals, new List<Instruction>(),
                    new List<ExceptionHandler>(), new List<Local>())
                {MaxStack = methodDef.Body.MaxStack};
                MapMethodCode(methodDef, mappedMethodDef);
                mappedMethodDef.Body.SimplifyMacros(mappedMethodDef.Parameters);
            }
        }

        private void MapMethodCode(MethodDef source, MethodDef target, int codeOffset = 0)
        {
            var bodyMapping = new Dictionary<object, object>();

            // for partial method injection
            foreach (var instruction in target.Body.Instructions)
            {
                bodyMapping[instruction] = instruction;
            }
            foreach (var variable in target.Body.Variables)
            {
                bodyMapping[variable] = variable;
            }

            foreach (var local in source.Body.Variables)
            {
                var mappedLocal = new Local(importer.Import(local.Type)) {Name = local.Name};
                target.Body.Variables.Add(mappedLocal);
                bodyMapping[local] = mappedLocal;
            }

            // remap outside references (Field, Methods, Types)
            foreach (var instruction in source.Body.Instructions)
            {
                var mappedInstruction = new Instruction(instruction.OpCode, instruction.Operand);
                if (mappedInstruction.Operand is IType)
                {
                    mappedInstruction.Operand = (IType) TryResolveImport(mappedInstruction.Operand) ??
                                                importer.Import((IType) mappedInstruction.Operand);
                }
                else if (mappedInstruction.Operand is IMethod)
                {
                    mappedInstruction.Operand = (IMethod) TryResolveImport(mappedInstruction.Operand) ??
                                                importer.Import((IMethod) mappedInstruction.Operand);
                }
                else if (mappedInstruction.Operand is IField)
                {
                    mappedInstruction.Operand = (IField) TryResolveImport(mappedInstruction.Operand) ??
                                                importer.Import((IField) mappedInstruction.Operand);
                }
                target.Body.Instructions.Insert(codeOffset++, mappedInstruction);
                bodyMapping[instruction] = mappedInstruction;
            }

            // remap internal (branches, switch)
            foreach (var instruction in target.Body.Instructions)
            {
                if (instruction.Operand != null && bodyMapping.ContainsKey(instruction.Operand))
                    instruction.Operand = bodyMapping[instruction.Operand];
                else if (instruction.Operand is Instruction[])
                    instruction.Operand =
                        ((Instruction[]) instruction.Operand).Select(t => (Instruction) bodyMapping[t])
                            .ToArray();
            }

            foreach (var exceptionHandler in source.Body.ExceptionHandlers)
            {
                target.Body.ExceptionHandlers.Add(new ExceptionHandler(exceptionHandler.HandlerType)
                {
                    CatchType =
                        exceptionHandler.CatchType == null
                            ? null
                            : ((ITypeDefOrRef) TryResolveImport(exceptionHandler.CatchType) ??
                               (ITypeDefOrRef) importer.Import(exceptionHandler.CatchType)),
                    TryStart = (Instruction) bodyMapping[exceptionHandler.TryStart],
                    TryEnd = (Instruction) bodyMapping[exceptionHandler.TryEnd],
                    HandlerStart = (Instruction) bodyMapping[exceptionHandler.HandlerStart],
                    HandlerEnd = (Instruction) bodyMapping[exceptionHandler.HandlerEnd],
                    FilterStart =
                        exceptionHandler.FilterStart == null
                            ? null
                            : (Instruction) bodyMapping[exceptionHandler.FilterStart]
                });
            }
        }

        private MethodDefUser Clone(MethodDef source)
        {
            var methodDef = new MethodDefUser(source.Name, null, source.ImplAttributes, source.Attributes);
            foreach (var genericParameter in methodDef.GenericParameters)
            {
                methodDef.GenericParameters.Add(new GenericParamUser(genericParameter.Number, genericParameter.Flags,
                    "-"));
            }
            return methodDef;
        }

        private void Copy(TypeDef typeDef)
        {
            var mappedTypeDef = (TypeDef) mapping[typeDef];
            mappedTypeDef.BaseType = (ITypeDefOrRef) TryResolveImport(typeDef.BaseType) ??
                                     (ITypeDefOrRef) importer.Import(typeDef.BaseType);
            foreach (var iface in typeDef.Interfaces)
            {
                mappedTypeDef.Interfaces.Add(
                    new InterfaceImplUser((ITypeDefOrRef) TryResolveImport(iface.Interface) ??
                                          (ITypeDefOrRef) importer.Import(iface.Interface)));
            }
        }

        private TypeDefUser Clone(TypeDef source)
        {
            var typeDef = new TypeDefUser(source.Name) {Attributes = source.Attributes};
            if (source.ClassLayout != null)
                typeDef.ClassLayout = new ClassLayoutUser(source.ClassLayout.PackingSize, source.ClassSize);
            foreach (var genericParameter in source.GenericParameters)
                typeDef.GenericParameters.Add(new GenericParamUser(genericParameter.Number, genericParameter.Flags, "-"));
            return typeDef;
        }

        private void Copy(FieldDef fieldDef)
        {
            var mappedFieldDef = (FieldDef) mapping[fieldDef];
            mappedFieldDef.Signature = importer.Import(fieldDef.Signature);
        }

        private FieldDefUser Clone(FieldDef source)
        {
            return new FieldDefUser(source.Name, null, source.Attributes);
        }

        private CallingConventionSig HandleSignature(CallingConventionSig sig)
        {
            var sigType = sig.GetType();
            if (sigType == typeof (FieldSig))
            {
                var fieldSig = sig as FieldSig;
                if (fieldSig != null && fieldSig.Type.ElementType == ElementType.Class)
                {
                    var typeDef = (fieldSig.Type as ClassOrValueTypeSig).TypeDefOrRef;
                    var importType = TryResolveImport(typeDef);
                    if (importType != null)
                        return new FieldSig(new ClassSig((ITypeDefOrRef) importType));
                }
            }
            else if (sigType == typeof (MethodSig))
            {
                var methodSig = sig as MethodSig;
                var newMethodSig = new MethodSig(sig.GetCallingConvention());
                foreach (var typeSig in methodSig.Params)
                {
                }
            }
            return importer.Import(sig);
        }

        // Imports has to be first check against injected module
        private IDnlibDef TryResolveImport<T>(T element)
        {
            var dnlibDef = element as IDnlibDef;
            if (dnlibDef != null && mapping.ContainsKey(dnlibDef))
            {
                return mapping[dnlibDef];
            }
            return null;
        }
    }
}