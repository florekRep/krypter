﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using dnlib.DotNet;
using dnlib.DotNet.Emit;

namespace Krypter.Injector
{
    public class PrimitiveInjector
    {
        private readonly ModuleDef targetModule;
        private Importer importer;

        public PrimitiveInjector(ModuleDef targetModule)
        {
            this.targetModule = targetModule;
            importer = new Importer(targetModule, ImporterOptions.TryToUseTypeDefs);
        }

        public static void InjectMethodCallToCCtor(MethodDef constructor, MethodDef methodToCall)
        {
            constructor.Body.Instructions.Insert(0, new Instruction(OpCodes.Call, methodToCall));
        }

        public static void SwapFieldForInt(FieldDef field, uint value, MethodDef method)
        {
            foreach (
                var instruction in method.Body.Instructions.Where(i => i.OpCode == OpCodes.Ldsfld && i.Operand == field)
                )
            {
                instruction.OpCode = OpCodes.Ldc_I4;
                instruction.Operand = (int) value;
            }
        }

        public static void SwapFieldForString(FieldDef field, string value, MethodDef method)
        {
            foreach (
                var instruction in method.Body.Instructions.Where(i => i.OpCode == OpCodes.Ldsfld && i.Operand == field)
                )
            {
                instruction.OpCode = OpCodes.Ldstr;
                instruction.Operand = value;
            }
        }

        public static void InjectByteArray(FieldDef arrayField, MethodDef initMethod, byte[] data)
        {
            new PrimitiveInjector(initMethod.Module).InjectByteArrayImpl(arrayField, initMethod, data);
        }

        public static MethodDef SwapReferenceInMethod(MethodDef method, object search, object swap)
        {
            foreach (var instruction in method.Body.Instructions)
            {
                if (search.Equals(instruction.Operand))
                    instruction.Operand = swap;
            }
            return method;
        }

        private void InjectByteArrayImpl(FieldDef arrayField, MethodDef initMethod, byte[] data)
        {
            var classWithLayout = AddClassWithLayout(data.Length);
            var fieldWithRVA = AddFieldWithInitVal(classWithLayout, arrayField, data);
            InjectInitalizationIntoMethod(initMethod, arrayField, fieldWithRVA, data.Length);
        }

        private TypeDef AddClassWithLayout(int size)
        {
            TypeDef classWithLayout = new TypeDefUser("layoutClass", importer.Import(typeof (ValueType)));
            classWithLayout.Attributes |= TypeAttributes.Sealed | TypeAttributes.ExplicitLayout;
            classWithLayout.ClassLayout = new ClassLayoutUser(1, (uint) size);
            targetModule.Types.Add(classWithLayout);
            return classWithLayout;
            ;
        }

        private FieldDef AddFieldWithInitVal(TypeDef layoutClass, FieldDef arrayField, byte[] data)
        {
            FieldDef fieldWithRVA = new FieldDefUser("field", new FieldSig(layoutClass.ToTypeSig()),
                FieldAttributes.Static | FieldAttributes.Assembly | FieldAttributes.HasFieldRVA);
            fieldWithRVA.InitialValue = data;
            arrayField.DeclaringType.Fields.Add(fieldWithRVA);
            return fieldWithRVA;
        }

        private void InjectInitalizationIntoMethod(MethodDef initMethod, FieldDef arrayField, FieldDef fieldWithRVA,
            int size)
        {
            var systemByte = importer.Import(typeof (Byte));
            //ITypeDefOrRef runtimeHelpers = importer.Import(typeof(System.Runtime.CompilerServices.RuntimeHelpers));
            var initArray =
                importer.Import(typeof (RuntimeHelpers).GetMethod("InitializeArray",
                    new[] {typeof (Array), typeof (RuntimeFieldHandle)}));

            var instrs = initMethod.Body.Instructions;
            instrs.Insert(0, new Instruction(OpCodes.Ldc_I4, size));
            instrs.Insert(1, new Instruction(OpCodes.Newarr, systemByte));
            instrs.Insert(2, new Instruction(OpCodes.Dup));
            instrs.Insert(3, new Instruction(OpCodes.Ldtoken, fieldWithRVA));
            instrs.Insert(4, new Instruction(OpCodes.Call, initArray));
            instrs.Insert(5, new Instruction(OpCodes.Stsfld, arrayField));
        }
    }
}