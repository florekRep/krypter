﻿using System;

namespace Krypter.Utils
{
    public static class BytePatterns
    {
        public static int LocateFirst(this byte[] self, byte[] candidate)
        {
            if (!IsEmptyLocate(self, candidate))
            {
                for (var i = 0; i < self.Length; i++)
                {
                    if (IsMatch(self, i, candidate))
                        return i;
                }
            }
            throw new KrypterException("Byte pattern replacement, pattern not found");
        }

        public static void FindFirstAndReplace(this byte[] self, byte[] pattern, byte[] replacement)
        {
            if (pattern.Length != replacement.Length)
                throw new KrypterException("Byte pattern replacement, arrays size doesn't match");

            var position = LocateFirst(self, pattern);
            Buffer.BlockCopy(replacement, 0, self, position, replacement.Length);
        }

        private static bool IsMatch(byte[] array, int position, byte[] candidate)
        {
            if (candidate.Length > (array.Length - position))
                return false;
            for (var i = 0; i < candidate.Length; i++)
                if (array[position + i] != candidate[i])
                    return false;
            return true;
        }

        private static bool IsEmptyLocate(byte[] array, byte[] candidate)
        {
            return array == null || candidate == null || array.Length == 0 || candidate.Length == 0 ||
                   candidate.Length > array.Length;
        }
    }
}