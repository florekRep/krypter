﻿using System.Collections.Generic;
using System.Linq;
using dnlib.DotNet;

namespace Krypter.Utils
{
    public static class DnlibExtension
    {
        public static IEnumerable<MethodDef> AllMethods(this ModuleDef module)
        {
            return module.Assembly.Modules.SelectMany(m => m.GetTypes()).SelectMany(t => t.Methods);
        }

        public static bool IsDelegate(this TypeDef type)
        {
            if (type.BaseType == null)
                return false;

            var fullName = type.BaseType.FullName;
            return fullName == "System.Delegate" || fullName == "System.MulticastDelegate";
        }
    }
}