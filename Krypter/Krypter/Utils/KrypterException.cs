﻿using System;

namespace Krypter.Utils
{
    public class KrypterException : Exception
    {
        public KrypterException(string message) : base(message)
        {
        }
    }
}