﻿using dnlib.DotNet;

namespace Krypter.Protections
{
    public interface IProtectionStage
    {
        string Name { get; }

        string ActionName { get; }

        ModuleDef Execute(ModuleDef module);
    }
}