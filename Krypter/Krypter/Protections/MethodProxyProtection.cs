﻿using System;
using System.Collections.Generic;
using System.Linq;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using dnlib.DotNet.Writer;
using Krypter.Injector;
using Krypter.Runtime;
using Krypter.Utils;
using Krypter.Writer;

namespace Krypter.Protections
{
    public class MethodProxyProtection : IProtectionStage
    {
        private readonly INameProvider nameProvider;
        private readonly RuntimeTypeProvider runtimeProvider;
        private readonly ModuleWriterListener writerListener;
        private ISet<CallingSite> callingSites;
        private MethodDef createProxyMethod;
        private uint key;
        private IDictionary<MethodCall, MethodDef> methodToBridgeMapping;
        private IDictionary<MethodCall, TypeDef> methodToProxyTypeMapping;
        private ModuleDef module;

        public MethodProxyProtection(RuntimeTypeProvider runtimeProvider, INameProvider nameProvider,
            ModuleWriterListener writerListener)
        {
            this.runtimeProvider = runtimeProvider;
            this.nameProvider = nameProvider;
            this.writerListener = writerListener;
        }

        public string Name => "Method calls protection";

        public string ActionName => "Ukrtywanie wywołań metod zewnęrznych";

        public ModuleDef Execute(ModuleDef module)
        {
            this.module = module;
            key = RandomGenerator.GenerateKey();
            var collected = CollectMethodsToProcess();
            var methodRefsToProcess = collected.Item1;
            callingSites = collected.Item2;

            methodToProxyTypeMapping = CreateMethodProxies(methodRefsToProcess);
            methodToBridgeMapping = CreateProxyInvocationBridges(methodToProxyTypeMapping);
            SwapMethodCalls();

            PrimitiveInjector.SwapFieldForInt(runtimeProvider.EncryptionKey, key,
                runtimeProvider.CreateProxyMethod(runtimeProvider.ProxyCreator));
            var injectedType = RuntimeInjector.Inject(runtimeProvider.ProxyCreator, module.GlobalType);
            createProxyMethod = runtimeProvider.CreateProxyMethod(injectedType);
            writerListener.OnWriteEvent += CreateDelegateInstances;
            return module;
        }

        private Tuple<ISet<MethodCall>, ISet<CallingSite>> CollectMethodsToProcess()
        {
            ISet<MethodCall> toProcess = new HashSet<MethodCall>();
            ISet<CallingSite> callingSites = new HashSet<CallingSite>();
            foreach (var method in module.AllMethods().Where(m => m.HasBody))
            {
                foreach (
                    var instruction in
                        method.Body.Instructions.Where(i => i.OpCode == OpCodes.Call || i.OpCode == OpCodes.Callvirt))
                {
                    var target = (IMethod) instruction.Operand;
                    if (CheckIfProcess(target) && !IsPrefixedCall(method.Body, instruction))
                    {
                        toProcess.Add(new MethodCall
                        {
                            Target = target,
                            IsVirtualCall = instruction.OpCode == OpCodes.Callvirt
                        });
                        callingSites.Add(new CallingSite {CallInstruction = instruction, Method = method});
                    }
                }
            }
            return Tuple.Create(toProcess, callingSites);
        }

        private bool IsPrefixedCall(CilBody body, Instruction instruction)
        {
            var instructionIndex = body.Instructions.IndexOf(instruction);
            return instructionIndex > 0 &&
                   body.Instructions[instructionIndex - 1].OpCode.OpCodeType == OpCodeType.Prefix;
        }

        private bool CheckIfProcess(IMethod callTarget)
        {
            if (callTarget is MethodSpec || callTarget.DeclaringType is TypeSpec || callTarget.Name == ".ctor" ||
                callTarget.Name == ".cctor")
                return false;
            var declType = callTarget.DeclaringType;
            if (declType.IsValueType && callTarget.MethodSig.HasThis)
                return false;
            var typedef = declType.ResolveTypeDef();
            if (typedef != null && typedef.IsDelegate())
                return false;
            if (callTarget.MethodSig.ParamsAfterSentinel != null && callTarget.MethodSig.ParamsAfterSentinel.Count > 0)
                return false;
            return callTarget is MemberRef;
        }

        private IDictionary<MethodCall, TypeDef> CreateMethodProxies(ISet<MethodCall> toProcess)
        {
            var originalMethodToProxyTypeMapping = new Dictionary<MethodCall, TypeDef>();
            foreach (var methodCall in toProcess)
            {
                var name = nameProvider.Name;
                var ctor = new MethodDefUser(".ctor",
                    MethodSig.CreateInstance(module.CorLibTypes.Void, module.CorLibTypes.Object,
                        module.CorLibTypes.IntPtr))
                {
                    Attributes =
                        MethodAttributes.Assembly | MethodAttributes.HideBySig | MethodAttributes.RTSpecialName |
                        MethodAttributes.SpecialName,
                    ImplAttributes = MethodImplAttributes.Runtime
                };
                var invoke = new MethodDefUser("Invoke", GetFixedDelegateSig(methodCall.Target).Clone())
                {
                    Attributes =
                        MethodAttributes.Assembly | MethodAttributes.HideBySig | MethodAttributes.Virtual |
                        MethodAttributes.NewSlot,
                    ImplAttributes = MethodImplAttributes.Runtime
                };
                invoke.MethodSig.HasThis = true;

                var proxyType = new TypeDefUser(name, module.CorLibTypes.GetTypeRef("System", "MulticastDelegate"))
                {
                    Attributes = TypeAttributes.NotPublic | TypeAttributes.Sealed,
                    Methods = {ctor, invoke}
                };
                var field = new FieldDefUser("proxy_" + name, new FieldSig(proxyType.ToTypeSig()),
                    FieldAttributes.Static | FieldAttributes.Assembly);
                proxyType.Fields.Add(field);
                module.Types.Add(proxyType);
                proxyType.FindOrCreateStaticConstructor(); // .cctor has to be present when MD is flushed

                originalMethodToProxyTypeMapping.Add(methodCall, proxyType);
            }
            return originalMethodToProxyTypeMapping;
        }

        private MethodSig GetFixedDelegateSig(IMethod method)
        {
            var paramTypes = method.MethodSig.Params;
            if (method.MethodSig.HasThis && !method.MethodSig.ExplicitThis)
            {
                paramTypes = new[] {module.CorLibTypes.Object}.Concat(method.MethodSig.Params).ToList();
            }
            return MethodSig.CreateStatic(method.MethodSig.RetType, paramTypes.ToArray());
        }

        private IDictionary<MethodCall, MethodDef> CreateProxyInvocationBridges(
            IDictionary<MethodCall, TypeDef> proxyTypeMapping)
        {
            var methodToMethodMapping = new Dictionary<MethodCall, MethodDef>();
            foreach (var methodCallToSwap in proxyTypeMapping.Keys)
            {
                var proxyType = proxyTypeMapping[methodCallToSwap];
                var staticDelegateField = proxyType.Fields.First(f => f.IsStatic);
                var proxyCallMethod = new MethodDefUser("call_" + proxyType.Name,
                    GetFixedDelegateSig(methodCallToSwap.Target).Clone())
                {
                    Attributes = MethodAttributes.PrivateScope | MethodAttributes.Static,
                    ImplAttributes = MethodImplAttributes.Managed | MethodImplAttributes.IL
                };
                proxyCallMethod.Body = new CilBody();
                proxyCallMethod.Body.Instructions.Add(Instruction.Create(OpCodes.Ldsfld, staticDelegateField));
                foreach (var param in proxyCallMethod.Parameters)
                {
                    proxyCallMethod.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg, param));
                }
                proxyCallMethod.Body.Instructions.Add(Instruction.Create(OpCodes.Call, proxyType.FindMethod("Invoke")));
                proxyCallMethod.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));

                proxyType.Methods.Add(proxyCallMethod);
                methodToMethodMapping.Add(methodCallToSwap, proxyCallMethod);
            }
            return methodToMethodMapping;
        }

        public void CreateDelegateInstances(object sender, ModuleWriterEvent evt)
        {
            var writer = sender as ModuleWriter;
            if (evt == ModuleWriterEvent.MDMemberDefRidsAllocated)
            {
                //SwapMethodCalls();
                CreateDelegateInitizalizers(writer);
            }
        }

        private void SwapMethodCalls()
        {
            foreach (var callingSite in callingSites)
            {
                var methodCall = new MethodCall
                {
                    Target = (IMethod) callingSite.CallInstruction.Operand,
                    IsVirtualCall = callingSite.CallInstruction.OpCode == OpCodes.Callvirt
                };
                callingSite.CallInstruction.OpCode = OpCodes.Call;
                callingSite.CallInstruction.Operand = methodToBridgeMapping[methodCall];
            }
        }

        private void CreateDelegateInitizalizers(ModuleWriter writer)
        {
            foreach (var mapping in methodToProxyTypeMapping)
            {
                var proxyType = mapping.Value;
                var originalMethodRid = writer.MetaData.GetToken(mapping.Key.Target).Raw;
                var proxyFieldRid = writer.MetaData.GetToken(proxyType.Fields.First(f => f.IsStatic)).Raw;
                var deleagetCctor = proxyType.FindOrCreateStaticConstructor();
                deleagetCctor.Body.Instructions.Insert(0, Instruction.Create(OpCodes.Call, createProxyMethod));
                deleagetCctor.Body.Instructions.Insert(0,
                    Instruction.CreateLdcI4(Convert.ToInt32(mapping.Key.IsVirtualCall)));
                deleagetCctor.Body.Instructions.Insert(0, Instruction.CreateLdcI4((int) (originalMethodRid ^ key)));
                deleagetCctor.Body.Instructions.Insert(0, Instruction.CreateLdcI4((int) (proxyFieldRid ^ key)));
            }
        }

        private sealed class MethodCall
        {
            public bool IsVirtualCall;
            public IMethod Target;

            public override bool Equals(object obj)
            {
                if (!(obj is MethodCall))
                    return false;
                var other = obj as MethodCall;
                return Target.Equals(other.Target) && IsVirtualCall == other.IsVirtualCall;
            }

            public override int GetHashCode()
            {
                return Target.GetHashCode() ^ IsVirtualCall.GetHashCode();
            }
        }

        private sealed class CallingSite
        {
            public Instruction CallInstruction;
            public MethodDef Method;

            public override bool Equals(object obj)
            {
                if (!(obj is CallingSite))
                    return false;
                var other = obj as CallingSite;
                return Method.Equals(other.Method) && CallInstruction.Equals(other.CallInstruction);
            }

            public override int GetHashCode()
            {
                return Method.GetHashCode() ^ CallInstruction.GetHashCode();
            }
        }
    }
}