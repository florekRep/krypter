﻿using dnlib.DotNet;
using Krypter.Injector;
using Krypter.Runtime;

namespace Krypter.Protections
{
    public class NativeKryptik : IProtectionStage
    {
        private readonly RuntimeTypeProvider runtimeProvider;

        public NativeKryptik(RuntimeTypeProvider runtimeProvider)
        {
            this.runtimeProvider = runtimeProvider;
        }

        public string Name => "Encrypts entire module with another native layer";

        public string ActionName => "Dodawanie warstwy szyfrującej - kod natywny";

        public ModuleDef Execute(ModuleDef module)
        {
            var newType = new TypeDefUser("_", "_");
            module.Types.Add(newType);
            var oepName = module.EntryPoint.DeclaringType.FullName + ":" + module.EntryPoint.Name;

            var krypterEntry = runtimeProvider.NativeKryptikEntry(runtimeProvider.NativeKryptik);
            PrimitiveInjector.SwapFieldForString(runtimeProvider.StringValue, oepName, krypterEntry);
            var injectedType = RuntimeInjector.Inject(runtimeProvider.NativeKryptik, newType);
            module.EntryPoint = runtimeProvider.NativeKryptikEntry(injectedType);

            return module;
        }
    }
}