﻿using System;
using System.Collections.Generic;
using System.Linq;
using dnlib.DotNet;
using dnlib.DotNet.Writer;
using Krypter.Injector;
using Krypter.Runtime;
using Krypter.Utils;
using Krypter.Writer;

namespace Krypter.Protections
{
    public class JITMethodEncryptionProtection : IProtectionStage
    {
        private readonly ModuleWriterListener moduleWriterListener;
        private readonly RuntimeTypeProvider runtimeProvider;
        private uint key;
        private ModuleDef module;

        public JITMethodEncryptionProtection(RuntimeTypeProvider runtimeProvider,
            ModuleWriterListener moduleWriterListener)
        {
            this.runtimeProvider = runtimeProvider;
            this.moduleWriterListener = moduleWriterListener;
        }

        public string Name => "Encrypts methods in assembly, decryption done at runtime";

        public string ActionName => "Szyfrowanie kodu metodą Just In Time";

        public ModuleDef Execute(ModuleDef module)
        {
            this.module = module;
            key = RandomGenerator.GenerateKey();
            var hookMethod = runtimeProvider.JitHookMethod(runtimeProvider.JitMethodDecryptor);
            PrimitiveInjector.SwapFieldForInt(runtimeProvider.EncryptionKey, key, hookMethod);
            var injectedType = RuntimeInjector.Inject(runtimeProvider.JitMethodDecryptor, module.GlobalType);
            var installHookMethod = runtimeProvider.JitInstallHookMethod(injectedType);
            PrimitiveInjector.InjectMethodCallToCCtor(injectedType.FindOrCreateStaticConstructor(), installHookMethod);
            moduleWriterListener.OnWriteEvent += OnWriterEvent;
            return module;
        }

        public void OnWriterEvent(object sender, ModuleWriterEvent evt)
        {
            var writer = sender as ModuleWriter;
            if (evt == ModuleWriterEvent.EndCalculateRvasAndFileOffsets)
            {
                var cctorBody = writer.MetaData.GetMethodBody(module.GlobalType.FindStaticConstructor());
                var decryptMethodBody =
                    writer.MetaData.GetMethodBody(runtimeProvider.JitInstallHookMethod(module.GlobalType));
                var hookMethodBody = writer.MetaData.GetMethodBody(runtimeProvider.JitHookMethod(module.GlobalType));
                var allMethodsBody =
                    module.AllMethods().Where(m => m.HasBody).Select(m => writer.MetaData.GetMethodBody(m));
                CryptMethods(allMethodsBody.Except(new[] {cctorBody, decryptMethodBody, hookMethodBody}).ToList());
            }
        }

        private void CryptMethods(IList<MethodBody> methodBodies)
        {
            var encryptionKeyBytes = BitConverter.GetBytes(key);
            foreach (var body in methodBodies)
            {
                var headerOffset = body.IsTiny ? 1 : 12;
                for (var i = headerOffset; i < body.Code.Length; i++)
                    body.Code[i] ^= encryptionKeyBytes[(i - headerOffset)%encryptionKeyBytes.Length];
            }
        }
    }
}