﻿using System;
using System.Collections.Generic;
using System.Linq;
using dnlib.DotNet;
using dnlib.DotNet.Writer;
using Krypter.Injector;
using Krypter.Runtime;
using Krypter.Utils;
using Krypter.Writer;

namespace Krypter.Protections
{
    public class StaticMethodEncryptionProtection : IProtectionStage
    {
        private readonly ModuleWriterListener moduleWriterListener;
        private readonly RuntimeTypeProvider runtimeProvider;
        private uint key;
        private ModuleDef module;

        public StaticMethodEncryptionProtection(RuntimeTypeProvider runtimeProvider,
            ModuleWriterListener moduleWriterListener)
        {
            this.runtimeProvider = runtimeProvider;
            this.moduleWriterListener = moduleWriterListener;
        }

        public string Name => "Encrypts methods in assembly, decryption done at load time";

        public string ActionName => "Szyfrowanie kodu metodą statyczną";

        public ModuleDef Execute(ModuleDef module)
        {
            this.module = module;
            key = RandomGenerator.GenerateKey();
            var decryptionMethod = runtimeProvider.StaticDecryptionMethod(runtimeProvider.StaticMethodDecryptor);
            PrimitiveInjector.SwapFieldForInt(runtimeProvider.EncryptionKey, key, decryptionMethod);

            var injectedType = RuntimeInjector.Inject(runtimeProvider.StaticMethodDecryptor, module.GlobalType);
            var injectedDecryptionMethod = runtimeProvider.StaticDecryptionMethod(injectedType);
            PrimitiveInjector.InjectMethodCallToCCtor(injectedType.FindOrCreateStaticConstructor(),
                injectedDecryptionMethod);
            moduleWriterListener.OnWriteEvent += OnWriterEvent;
            return module;
        }

        public void OnWriterEvent(object sender, ModuleWriterEvent evt)
        {
            var writer = sender as ModuleWriter;
            if (evt == ModuleWriterEvent.EndCalculateRvasAndFileOffsets)
            {
                var cctorBody = writer.MetaData.GetMethodBody(module.GlobalType.FindStaticConstructor());
                var decryptMethodBody =
                    writer.MetaData.GetMethodBody(runtimeProvider.StaticDecryptionMethod(module.GlobalType));
                var allMethodsBody =
                    module.AllMethods().Where(m => m.HasBody).Select(m => writer.MetaData.GetMethodBody(m)).ToList();
                var methodsRange = GetMethodsRange(allMethodsBody);

                CryptMethods(allMethodsBody.Except(new[] {cctorBody, decryptMethodBody}).ToList(), methodsRange);
                PatchOffsets(decryptMethodBody, methodsRange.Item1, methodsRange.Item2, (uint) decryptMethodBody.RVA,
                    decryptMethodBody.GetVirtualSize(), (uint) cctorBody.RVA, cctorBody.GetVirtualSize());
            }
        }

        private void CryptMethods(IList<MethodBody> methodBodies, Tuple<uint, uint> range)
        {
            var allMethodsStart = range.Item1;
            var encryptionKeyBytes = BitConverter.GetBytes(key);
            foreach (var body in methodBodies)
            {
                var methodStart = (uint) body.RVA;
                for (var i = 0; i < body.Code.Length; i++)
                    body.Code[i] ^= encryptionKeyBytes[(methodStart - allMethodsStart + i)%encryptionKeyBytes.Length];
                if (body.HasExtraSections)
                {
                    // extra section are aligned to next 4 byte address.
                    var startOffset = methodStart - allMethodsStart + body.Code.Length +
                                      ((body.Code.Length%4 == 0) ? 0 : 4 - body.Code.Length%4);
                    for (var i = 0; i < body.ExtraSections.Length; i++)
                        body.ExtraSections[i] ^= encryptionKeyBytes[(startOffset + i)%encryptionKeyBytes.Length];
                }
            }
        }

        private Tuple<uint, uint> GetMethodsRange(IList<MethodBody> methodBodies)
        {
            var methodsStart = uint.MaxValue;
            var methodsEnd = uint.MinValue;

            foreach (var body in methodBodies)
            {
                if ((uint) body.RVA < methodsStart)
                {
                    methodsStart = (uint) body.RVA;
                }
                if ((uint) body.RVA + body.GetVirtualSize() > methodsEnd)
                {
                    methodsEnd = (uint) body.RVA + body.GetVirtualSize();
                }
            }
            return Tuple.Create(methodsStart, methodsEnd - methodsStart);
        }

        private void PatchOffsets(MethodBody method, uint methodsStart, uint methodsSize, uint decryptStart,
            uint decryptSize, uint cctorStart, uint cctorSize)
        {
            method.Code.FindFirstAndReplace(BitConverter.GetBytes((uint) 0x0FF53710),
                BitConverter.GetBytes(methodsStart));
            method.Code.FindFirstAndReplace(BitConverter.GetBytes((uint) 0x05173100),
                BitConverter.GetBytes(methodsSize));
            method.Code.FindFirstAndReplace(BitConverter.GetBytes((uint) 0x0FF53720),
                BitConverter.GetBytes(decryptStart));
            method.Code.FindFirstAndReplace(BitConverter.GetBytes((uint) 0x05173200),
                BitConverter.GetBytes(decryptSize));
            method.Code.FindFirstAndReplace(BitConverter.GetBytes((uint) 0x0FF53730),
                BitConverter.GetBytes(cctorStart));
            method.Code.FindFirstAndReplace(BitConverter.GetBytes((uint) 0x05173300),
                BitConverter.GetBytes(cctorSize));
        }
    }
}