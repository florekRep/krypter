﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using dnlib.DotNet.Writer;
using Krypter.Injector;
using Krypter.Runtime;
using Krypter.Writer;

namespace Krypter.Protections
{
    public class ManagedKryptik : IProtectionStage
    {
        private readonly INameProvider nameProvider;
        private readonly RuntimeTypeProvider runtimeProvider;
        private readonly ModuleWriterListener writerListener;
        private ModuleDef stub;

        public ManagedKryptik(RuntimeTypeProvider runtimeProvider, ModuleWriterListener writerListener,
            INameProvider nameProvider)
        {
            this.runtimeProvider = runtimeProvider;
            this.writerListener = writerListener;
            this.nameProvider = nameProvider;
        }

        public string Name => "Encrypts entire module with another layer";

        public string ActionName => "Dodawanie warstwy szyfrującej - kod zarządzany";

        public ModuleDef Execute(ModuleDef module)
        {
            var originalModuleData = WriteOriginalModule(module);
            stub = CreateStubModule("stub", module);
            module.Assembly.Modules.Insert(0, stub);
            var injectedStubRuntime = RuntimeInjector.Inject(runtimeProvider.Kryptik, stub.GlobalType);
            stub.EntryPoint = injectedStubRuntime.Methods.First(m => m.Name == "Main");
            ComposeDecrypterLayer(injectedStubRuntime, originalModuleData);
            return stub;
        }

        private void ComposeDecrypterLayer(TypeDef injectedRuntime, byte[] rawModule)
        {
            var rand = new Random();
            var data = rawModule;

            data = Random2ndLayer(injectedRuntime, data, rand);
            data = Random1stLayer(injectedRuntime, data, rand);
            RandomDataLoadMethod(injectedRuntime, data, rand);
        }

        private byte[] Random2ndLayer(TypeDef injectedRuntime, byte[] data, Random rand)
        {
            var cryptoMethod = runtimeProvider.CryptoMethod(injectedRuntime);
            var operations = new[] {OpCodes.Xor, OpCodes.Sub, OpCodes.Add};
            var reversedOperations = new Func<byte, byte, byte>[]
            {(a, b) => (byte) (a ^ b), (a, b) => (byte) (a + b), (a, b) => (byte) (a - b)};
            var transforms = new List<Func<byte, byte, byte>>();

            foreach (var instruction in cryptoMethod.Body.Instructions.Where(i => i.OpCode == OpCodes.Xor))
            {
                var idx = rand.Next(3);
                instruction.OpCode = operations[idx];
                transforms.Add(reversedOperations[idx]);
            }

            transforms.Reverse();
            for (var i = 0; i < data.Length; i++)
            {
                foreach (var transform in transforms)
                {
                    data[i] = transform(data[i], (byte) i);
                }
            }
            return data;
        }

        private byte[] Random1stLayer(TypeDef injectedRuntime, byte[] data, Random rand)
        {
            switch (rand.Next(3))
            {
                case 0:
                    data = Encoding.UTF8.GetBytes(Convert.ToBase64String(data));
                    Base64Encryption(injectedRuntime);
                    break;
                case 1:
                    using (var outputStream = new MemoryStream())
                    {
                        using (var zipStream = new DeflateStream(outputStream, CompressionMode.Compress))
                        {
                            zipStream.Write(data, 0, data.Length);
                        }
                        data = outputStream.ToArray();
                    }
                    DeflateEncryption(injectedRuntime);
                    break;
                case 2:
                    var salt = new byte[16];
                    rand.NextBytes(salt);
                    var pass = nameProvider.Name;
                    var rijndael = Rijndael.Create();
                    var pdb = new Rfc2898DeriveBytes(pass, salt);
                    rijndael.Key = pdb.GetBytes(32);
                    rijndael.IV = pdb.GetBytes(16);
                    using (var memoryStream = new MemoryStream())
                    {
                        using (
                            var cryptoStream = new CryptoStream(memoryStream, rijndael.CreateEncryptor(),
                                CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(data, 0, data.Length);
                        }
                        data = memoryStream.ToArray();
                    }
                    AesEncryption(injectedRuntime, salt, pass);
                    break;
            }
            return data;
        }

        private void RandomDataLoadMethod(TypeDef injectedRuntime, byte[] data, Random rand)
        {
            switch (rand.Next(2))
            {
                case 0:
                    StoreInArray(injectedRuntime, data);
                    break;
                case 1:
                    StoreInResources(injectedRuntime, data);
                    break;
            }
        }

        private void Base64Encryption(TypeDef injected)
        {
            var base64Decode = PrimitiveInjector.SwapReferenceInMethod(
                runtimeProvider.Helper.FindMethod("Base64Decode"), runtimeProvider.Kryptik.FindField("encrypted"),
                injected.FindField("encrypted"));
            RuntimeInjector.Inject(base64Decode, injected.FindMethod("Main"), 0);
        }

        private void DeflateEncryption(TypeDef injected)
        {
            var deflateDecode =
                PrimitiveInjector.SwapReferenceInMethod(runtimeProvider.Helper.FindMethod("DeflateDecode"),
                    runtimeProvider.Kryptik.FindField("encrypted"), injected.FindField("encrypted"));
            RuntimeInjector.Inject(deflateDecode, injected.FindMethod("Main"), 0);
        }

        private void AesEncryption(TypeDef injected, byte[] salt, string pass)
        {
            if (salt.Length != 16)
                throw new ArgumentException("Salt has to be 16 bytes");

            var saltChars = new char[salt.Length/sizeof (char)];
            Buffer.BlockCopy(salt, 0, saltChars, 0, salt.Length);
            var passAndSalt = new string(saltChars) + pass;
            var aesDecode = PrimitiveInjector.SwapReferenceInMethod(runtimeProvider.Helper.FindMethod("AesDecode"),
                runtimeProvider.Kryptik.FindField("encrypted"), injected.FindField("encrypted"));
            PrimitiveInjector.SwapFieldForString(runtimeProvider.StringValue, passAndSalt, aesDecode);
            RuntimeInjector.Inject(aesDecode, injected.FindMethod("Main"), 0);
        }

        private void StoreInArray(TypeDef injected, byte[] data)
        {
            PrimitiveInjector.InjectByteArray(injected.GetField("encrypted"), injected.FindMethod("Main"), data);
        }

        private void StoreInResources(TypeDef injected, byte[] data)
        {
            var resourceName = nameProvider.Name;
            stub.Resources.Add(new EmbeddedResource(resourceName, data));
            var extractFromResource =
                PrimitiveInjector.SwapReferenceInMethod(runtimeProvider.Helper.FindMethod("ExtractDataFromResource"),
                    runtimeProvider.Kryptik.FindField("encrypted"), injected.FindField("encrypted"));
            PrimitiveInjector.SwapFieldForString(runtimeProvider.StringValue, resourceName, extractFromResource);
            RuntimeInjector.Inject(extractFromResource, injected.FindMethod("Main"), 0);
        }

        private ModuleDefUser CreateStubModule(string name, ModuleDef originalModule)
        {
            return new ModuleDefUser(name, originalModule.Mvid, originalModule.CorLibTypes.AssemblyRef)
            {
                Characteristics = originalModule.Characteristics,
                Cor20HeaderFlags = originalModule.Cor20HeaderFlags,
                Cor20HeaderRuntimeVersion = originalModule.Cor20HeaderRuntimeVersion,
                DllCharacteristics = originalModule.DllCharacteristics,
                EncBaseId = originalModule.EncBaseId,
                EncId = originalModule.EncId,
                Generation = originalModule.Generation,
                Kind = originalModule.Kind,
                Machine = originalModule.Machine,
                RuntimeVersion = originalModule.RuntimeVersion,
                TablesHeaderVersion = originalModule.TablesHeaderVersion,
                Win32Resources = originalModule.Win32Resources
            };
        }

        private byte[] WriteOriginalModule(ModuleDef module)
        {
            var options = new ModuleWriterOptions(module, writerListener);
            var moduleWriter = new ModuleWriter(module, options);
            byte[] data;
            using (var memoryStream = new MemoryStream())
            {
                moduleWriter.Write(memoryStream);
                data = memoryStream.ToArray();
            }
            writerListener.Reset();
            return data;
        }
    }
}