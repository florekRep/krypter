﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dnlib.DotNet;
using dnlib.DotNet.Emit;
using Krypter.Injector;
using Krypter.Protections;
using Krypter.Runtime;
using Krypter.Utils;

namespace Krypter
{
    public class StringEncryptionProtection : IProtectionStage
    {
        private readonly RuntimeTypeProvider runtimeProvider;
        private ModuleDef module;

        public StringEncryptionProtection(RuntimeTypeProvider runtimeProvider)
        {
            this.runtimeProvider = runtimeProvider;
        }

        public string Name => "String Protection (Encryption)";

        public string ActionName => "Ukrtwanie łańcuchów znaków";

        public ModuleDef Execute(ModuleDef module)
        {
            this.module = module;
            var key = RandomGenerator.GenerateKey();
            var ldstrs = CollectStringReferences();
            var encryptedArray = EncryptStrings(CollectStrings(ldstrs), key);
            var decodeKeyField = runtimeProvider.EncryptionKey;
            var decodeMethod = runtimeProvider.StringDecryptionMethod(runtimeProvider.StringDecryptor);
            PrimitiveInjector.SwapFieldForInt(decodeKeyField, key, decodeMethod);

            var injectedType = RuntimeInjector.Inject(runtimeProvider.StringDecryptor, module.GlobalType);
            var injectedInitMethod = runtimeProvider.StringEncryptedArrayInitMethod(injectedType);
            var injectedDecodeMethod = runtimeProvider.StringDecryptionMethod(injectedType);
            var arrayField = runtimeProvider.StringEncryptedArray(injectedType);
            PrimitiveInjector.InjectByteArray(arrayField, injectedInitMethod, encryptedArray);
            PrimitiveInjector.InjectMethodCallToCCtor(injectedType.FindOrCreateStaticConstructor(), injectedInitMethod);
            var changedMethods = PatchStringReferences(ldstrs, injectedDecodeMethod);

            //short br instruction become corrupted after ldc with int64
            foreach (var method in changedMethods)
            {
                method.Body.SimplifyBranches();
            }
            return module;
        }

        private ISet<MethodDef> PatchStringReferences(IList<Tuple<MethodDef, Instruction>> ldstrs,
            MethodDef decodingFunction)
        {
            uint counter = 0;
            var changedMethods = new HashSet<MethodDef>();
            foreach (var loadStringRef in ldstrs)
            {
                var method = loadStringRef.Item1;
                var instr = loadStringRef.Item2;
                var stringLen = ((string)instr.Operand).Length;
                var indexKey = ((ulong)counter) << 32 | (ulong)stringLen;

                var i = method.Body.Instructions.IndexOf(instr);
                instr.OpCode = OpCodes.Ldc_I8;
                instr.Operand = (long)indexKey;
                method.Body.Instructions.Insert(i + 1, Instruction.Create(OpCodes.Call, decodingFunction));
                changedMethods.Add(method);
                counter += (uint)stringLen;
            }
            return changedMethods;
        }

        private IList<Tuple<MethodDef, Instruction>> CollectStringReferences()
        {
            var ldstrs = new List<Tuple<MethodDef, Instruction>>();
            foreach (var method in module.AllMethods().Where(m => m.HasBody))
            {
                ldstrs.AddRange(method.Body.Instructions.Where(i => i.OpCode == OpCodes.Ldstr && !i.Operand.Equals(""))
                    .Select(i => Tuple.Create(method, i)));
            }
            return ldstrs;
        }

        private byte[] CollectStrings(IList<Tuple<MethodDef, Instruction>> ldstrs)
        {
            var strings = ldstrs.Select(loadStringRef => (string)loadStringRef.Item2.Operand).ToList();
            return strings.SelectMany(s => Encoding.UTF8.GetBytes(s)).ToArray();
        }

        private byte[] EncryptStrings(byte[] stringsArray, uint key)
        {
            var encryptionKeyBytes = BitConverter.GetBytes(key);
            for (var i = 0; i < stringsArray.Length; i++)
            {
                stringsArray[i] = (byte)(stringsArray[i] ^ encryptionKeyBytes[i % encryptionKeyBytes.Length]);
            }
            return stringsArray;
        }
    }
}