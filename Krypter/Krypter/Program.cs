﻿using System;
using System.IO;
using System.Linq;
using dnlib.DotNet;
using Krypter.Protections;
using Krypter.Runtime;
using Krypter.Writer;

namespace Krypter
{
    public class Program
    {
        private Pipeline pipeline;
        private IProtectedModuleWriter writer;


        private const string TEST_PATH = @"d:\studia\mgr\protectors\ConfuserEx_bin\Confuser.CLI.exe"; //@"d:\studia\mgr\example\helloworld.exe"; // 
        private const string SAVE_PATH = @"d:\studia\mgr\example\modified.exe";
        private const string RUNTIME = @"Runtime.dll";
        private const string STUB = @"NativeStub.exe";


        public static void Usage()
        {
            Console.WriteLine("Usage: Krypter [opcje] plik_wejsciowy plik_wyjsciowy");
            Console.WriteLine("Dostępne opcje: ");
            Console.WriteLine("/string - szyfrowanie ciągów znaków");
            Console.WriteLine("/method - ukrywanie wywołań metod");
            Console.WriteLine("/jit - szyfrowanie kodu metodą JIT");
            Console.WriteLine("/static - szyfrowanie kodu metodą statyczna");
            Console.WriteLine("/managed - dodatkowa warstwa szyfrująca");
            Console.WriteLine("/native - natywna warstwa szyfrująca");
        }

        public static void Run(Pipeline pipeline, IProtectedModuleWriter writer, string input, string output)
        {
            Console.WriteLine("[*] Przetwarania pliku " + input);
            var target = ModuleDefMD.Load(input);
            var protectedModule = pipeline.Run(target);
            writer.Write(protectedModule, output);
            Console.WriteLine("[*] Zabezpieczona wersja zapisana w: " + output);
        }

        public static void ParseArguments(string[] args)
        {
            if (args.Length < 3)
            {
                Usage();
                Environment.Exit(1);
            }
            var output = args[args.Length - 1];
            var input = args[args.Length - 2];

            var runtimeProvider = new RuntimeTypeProvider(RUNTIME);
            var nameProvider = new RandomNameProvider();
            var writerListener = new ModuleWriterListener();
            var pipeline = new Pipeline();
            IProtectedModuleWriter writer = new ListenerModuleWriter(writerListener); ;
            if (args.Contains("/string"))
            {
                var stringEncryption = new StringEncryptionProtection(runtimeProvider);
                pipeline.AddStage(stringEncryption);
            }
            if (args.Contains("/method"))
            {
                var methodProxy = new MethodProxyProtection(runtimeProvider, nameProvider, writerListener);
                pipeline.AddStage(methodProxy);
            }
            if (args.Contains("/jit"))
            {
                if (args.Contains("/static"))
                {
                    Console.WriteLine("Nie można jednocześnie użyć szyfrowania kodu statycznego oraz JIT");
                    Environment.Exit(2);
                }
                var jitMethodEncryption = new JITMethodEncryptionProtection(runtimeProvider, writerListener);
                pipeline.AddStage(jitMethodEncryption);
            }
            if (args.Contains("/static"))
            {
                if (args.Contains("/jit"))
                {
                    Console.WriteLine("Nie można jednocześnie użyć szyfrowania kodu statycznego oraz JIT");
                    Environment.Exit(2);
                }
                var staticMethodEncryption = new StaticMethodEncryptionProtection(runtimeProvider, writerListener);
                pipeline.AddStage(staticMethodEncryption);
            }
            if (args.Contains("/managed"))
            {
                if (args.Contains("/native"))
                {
                    Console.WriteLine("Nie można jednocześnie użyć warsztwy szyfrującej natywnej oraz .NETowej");
                    Environment.Exit(2);
                }
                writer = new NetKryptikWriter();
                var netKryptik = new ManagedKryptik(runtimeProvider, writerListener, nameProvider);
                pipeline.AddStage(netKryptik);
            }
            if (args.Contains("/native"))
            {
                if (args.Contains("/managed"))
                {
                    Console.WriteLine("Nie można jednocześnie użyć warsztwy szyfrującej natywnej oraz .NETowej");
                    Environment.Exit(2);
                }
                writer = new NativeKryptikWriter(writerListener, STUB);
                var nativeKryptik = new NativeKryptik(runtimeProvider);
                pipeline.AddStage(nativeKryptik);
            }

            Run(pipeline, writer, input, output);

        }

        public static void Main(string[] args)
        {
            Console.WriteLine(Directory.GetCurrentDirectory());
            ParseArguments(args);
        }
    }
}