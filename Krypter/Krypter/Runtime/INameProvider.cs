﻿namespace Krypter.Runtime
{
    public interface INameProvider
    {
        string Name { get; }
    }
}