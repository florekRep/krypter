﻿using System.Linq;
using dnlib.DotNet;

namespace Krypter.Runtime
{
    public class RuntimeTypeProvider
    {
        private readonly ModuleDefMD runtimeModule;

        public RuntimeTypeProvider(string runtimeDll)
        {
            runtimeModule = ModuleDefMD.Load(runtimeDll);
        }

        public TypeDef StringDecryptor => runtimeModule.Types.First(t => t.Name == "StringDecryptor");
        public TypeDef StaticMethodDecryptor => runtimeModule.Types.First(t => t.Name == "StaticMethodDecryptor");
        public TypeDef JitMethodDecryptor => runtimeModule.Types.First(t => t.Name == "JitMethodDecryptor");
        public TypeDef ProxyCreator => runtimeModule.Types.First(t => t.Name == "MethodRefProxy");
        public TypeDef Kryptik => runtimeModule.Types.First(t => t.Name == "Kryptik");
        public TypeDef NativeKryptik => runtimeModule.Types.First(t => t.Name == "NativeKryptik");
        public TypeDef Helper => runtimeModule.Types.First(t => t.Name == "Helper");
        public FieldDef EncryptionKey => Helper.FindField("key");
        public FieldDef StringValue => Helper.FindField("stringValue");
        public FieldDef StringEncryptedArray(TypeDef type) => type.FindField("encrypted");
        public FieldDef OepMethodName(TypeDef type) => type.FindField("oepMethodName");
        public MethodDef StringDecryptionMethod(TypeDef type) => type.FindMethod("GetString");
        public MethodDef StringEncryptedArrayInitMethod(TypeDef type) => type.FindMethod("InitArray");
        public MethodDef StaticDecryptionMethod(TypeDef type) => type.FindMethod("Decrypt");
        public MethodDef JitInstallHookMethod(TypeDef type) => type.FindMethod("InstallHook");
        public MethodDef JitHookMethod(TypeDef type) => type.FindMethod("HookedCompileMethod");
        public MethodDef CreateProxyMethod(TypeDef type) => type.FindMethod("CreateDelegate");
        public MethodDef CryptoMethod(TypeDef type) => type.FindMethod("Crypto");
        public MethodDef NativeKryptikEntry(TypeDef type) => type.FindMethod("EP");
    }
}