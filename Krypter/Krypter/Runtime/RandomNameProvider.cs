﻿using System;

namespace Krypter.Runtime
{
    public class RandomNameProvider : INameProvider
    {
        private readonly Random rand = new Random();

        public string Name
        {
            get
            {
                var randomness = new byte[8];
                rand.NextBytes(randomness);
                return Convert.ToBase64String(randomness);
            }
        }
    }
}