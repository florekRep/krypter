﻿namespace Krypter.Runtime
{
    public class PrefixSeqNameProvider : INameProvider
    {
        private readonly string prefix;
        private int counter;

        public PrefixSeqNameProvider(string prefix, int counter = 0)
        {
            this.prefix = prefix;
            this.counter = counter;
        }

        public string Name => string.Format("{0}{1}", prefix, counter++);
    }
}