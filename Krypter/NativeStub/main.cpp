#include <metahost.h>
#include <memory>
#include <windows.h>

#pragma comment(lib, "mscoree.lib")

#import "mscorlib.tlb" raw_interfaces_only				\
    high_property_prefixes("_get","_put","_putref")		\
    rename("ReportEvent", "InteropServices_ReportEvent")

using namespace mscorlib;
using namespace std;

static shared_ptr<char> LoadDecryptedFile(int* size) {
	HRSRC programResource = FindResource(nullptr, L"code", RT_RCDATA);
	*size = SizeofResource(nullptr, programResource);
	HGLOBAL programResourceHandle = LoadResource(nullptr, programResource);
	char* programData = (char*)LockResource(programResourceHandle);

	HRSRC keyResource = FindResource(nullptr, L"key", RT_RCDATA);
	int keysize = SizeofResource(nullptr, keyResource);
	HGLOBAL keyResourceHandle = LoadResource(nullptr, keyResource);
	char* keyData = (char*)LockResource(keyResourceHandle);

	shared_ptr<char> decrypted(new char[*size], [](char *p) { delete[] p; });
	for (int i = 0; i < *size; i++) {
		decrypted.get()[i] = programData[i] ^ keyData[i % keysize];
	}
	return decrypted;
}

int main(int argc, char** argv) {
	//init clr
	ICLRMetaHost* pMetaHost = nullptr;
	CLRCreateInstance(CLSID_CLRMetaHost, IID_PPV_ARGS(&pMetaHost));
	shared_ptr<ICLRMetaHost> metaHost(pMetaHost, [](ICLRMetaHost* ptr) {ptr->Release();});

	ICLRRuntimeInfo *pRuntimeInfo = nullptr;
	metaHost->GetRuntime(L"v4.0.30319", IID_PPV_ARGS(&pRuntimeInfo));
	shared_ptr<ICLRRuntimeInfo> runtimeInfo(pRuntimeInfo, [](ICLRRuntimeInfo* ptr) {ptr->Release();});

	ICorRuntimeHost *pCorRuntimeHost = nullptr;
	runtimeInfo->GetInterface(CLSID_CorRuntimeHost, IID_PPV_ARGS(&pCorRuntimeHost));
	shared_ptr<ICorRuntimeHost> corRuntimeHost(pCorRuntimeHost, [](ICorRuntimeHost* ptr) {ptr->Release();});
	corRuntimeHost->Start();

	// get default domain
	IUnknownPtr spAppDomainThunk = nullptr;
	corRuntimeHost->GetDefaultDomain(&spAppDomainThunk);
	_AppDomainPtr spDefaultAppDomain = nullptr;
	spAppDomainThunk->QueryInterface(IID_PPV_ARGS(&spDefaultAppDomain));

	//load assembly
	int size = 0;
	shared_ptr<char> programData = LoadDecryptedFile(&size);
	SAFEARRAYBOUND bounds;
	bounds.lLbound = 0;
	bounds.cElements = size;

	shared_ptr<SAFEARRAY> assemblyLoadSafeArrayData(SafeArrayCreate(VT_UI1, 1, &bounds), [](SAFEARRAY* ptr) {SafeArrayDestroy(ptr);});
	void * arrayWriteAddr;
	SafeArrayAccessData(assemblyLoadSafeArrayData.get(), &arrayWriteAddr);
	memcpy(arrayWriteAddr, programData.get(), size);
	SafeArrayUnaccessData(assemblyLoadSafeArrayData.get());

	_AssemblyPtr spAssembly = nullptr;
	spDefaultAppDomain->Load_3(assemblyLoadSafeArrayData.get(), &spAssembly);

	_MethodInfoPtr spEntryPoint = nullptr;
	spAssembly->get_EntryPoint(&spEntryPoint);

	shared_ptr<SAFEARRAY> args(SafeArrayCreateVector(VT_VARIANT, 0, 0), [](SAFEARRAY* ptr) {SafeArrayDestroy(ptr);});
	variant_t vtRet;
	variant_t vtEmpty;
	spEntryPoint->Invoke_3(vtEmpty, args.get(), &vtRet);
}


