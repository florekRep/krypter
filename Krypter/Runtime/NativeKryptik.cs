﻿using System;
using System.Reflection;

namespace Runtime
{
    internal class NativeKryptik
    {
        public static void EP()
        {
            var args = Helper.stringValue.Split(':');
            var oep = typeof (NativeKryptik).Module.GetType(args[0])
                .GetMethod(args[1], BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
            if (oep.GetParameters().Length > 0)
            {
                var commandLine = Environment.GetCommandLineArgs();
                if (commandLine.Length > 0)
                {
                    var tmp = new string[commandLine.Length - 1];
                    Array.Copy(commandLine, 1, tmp, 0, commandLine.Length - 1);
                    commandLine = tmp;
                }
                oep.Invoke(null, new object[]
                {
                    commandLine
                });
                return;
            }
            oep.Invoke(null, new object[0]);
        }
    }
}