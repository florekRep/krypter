﻿using System;
using System.Runtime.InteropServices;

namespace Runtime
{
    internal static class StaticMethodDecryptor
    {
        [DllImport("kernel32.dll")]
        private static extern bool VirtualProtect(IntPtr lpAddress, uint dwSize, uint flNewProtect,
            out uint lpflOldProtect);

        public static unsafe void Decrypt()
        {
            uint methodsStart = 0x0FF53710;
            uint methodsSize = 0x05173100;
            uint decryptStart = 0x0FF53720;
            uint decryptSize = 0x05173200;
            uint cctorStart = 0x0FF53730;
            uint cctorSize = 0x05173300;

            var module = typeof (StaticMethodDecryptor).Module;
            var imageStart = (byte*) Marshal.GetHINSTANCE(module);
            var optionalHeader = imageStart + *(uint*) (imageStart + 0x3c);
            var sizeOfOptionalHeader = *(ushort*) (optionalHeader + 0x14);
            var sectionHeader = (uint*) (optionalHeader + 0x18 + sizeOfOptionalHeader);
            var sectionVirtualSize = *(sectionHeader + 2);
            var sectionVirtualAddress = *(sectionHeader + 3);
            var sectionRawSize = *(sectionHeader + 4);
            var sectionRawAddress = *(sectionHeader + 5);
            var isKrypterLoaded = module.Name[0] == '<';

            var fixedStartOffset = (isKrypterLoaded
                ? (imageStart - sectionVirtualAddress + sectionRawAddress)
                : imageStart);
            uint rights = 0x40;
            var virutalProtRes = VirtualProtect((IntPtr) (fixedStartOffset + methodsStart), methodsSize, rights,
                out rights);

#if RUNTIME_VERBOSE
            Console.WriteLine("[*][StaticMethodDecryptor] Module: {0} addr: {1:X} kryptik: {2} VP res: {3}", module.Name, (int)imageStart, isKrypterLoaded, virutalProtRes);
            Console.WriteLine("[*][StaticMethodDecryptor] Section data: VA: {0:X} size: {1:X}, raw: {2:X} size: {3:X}", (int)sectionVirtualAddress, sectionVirtualSize, (int)sectionRawAddress, sectionRawSize);
            Console.WriteLine("[*][StaticMethodDecryptor] Methods start: {0:X} size: {1:X} decrypt offset: {2:X}", methodsStart, methodsSize, (int)(fixedStartOffset + methodsStart));
#endif

            var decryptionKeyBytes = BitConverter.GetBytes(Helper.key);
            for (var i = methodsStart; i < methodsStart + methodsSize; i++)
            {
                if (i == decryptStart) i += decryptSize;
                if (i == cctorStart) i += cctorSize;
                *(fixedStartOffset + i) ^= decryptionKeyBytes[i%decryptionKeyBytes.Length];
            }
        }
    }
}