﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Runtime
{
    internal static class StringDecryptor
    {
        private static byte[] encrypted;
        private static readonly Dictionary<uint, string> cache = new Dictionary<uint, string>();

        public static void InitArray()
        {
            ;
        }

        public static string GetString(ulong key)
        {
            var index = (uint) (key >> 32);
            var length = (uint) (key & 0x00000000FFFFFFFF);
            string result;
            if (!cache.TryGetValue(index, out result))
            {
                var decryptionKeyBytes = BitConverter.GetBytes(Helper.key);
                var encryptedString = new byte[length];
                Array.Copy(encrypted, index, encryptedString, 0, length);
                for (var i = 0; i < length; i++)
                {
                    encryptedString[i] =
                        (byte) (encryptedString[i] ^ decryptionKeyBytes[(index + i)%decryptionKeyBytes.Length]);
                    result = Encoding.UTF8.GetString(encryptedString);
                    cache[index] = result;
                }
            }
#if RUNTIME_VERBOSE
            Console.WriteLine("[*][StringDecryptor] key: {0:X} idx: {1} string: {2}", key, index, result);
#endif
            return result;
        }
    }
}