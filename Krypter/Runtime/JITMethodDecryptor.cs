﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Runtime
{
    internal static unsafe class JitMethodDecryptor
    {
        private static Delegate originalCompileMethod;
        private static Delegate hookedDelegate;
        private static IntPtr moduleRuntimeHandle;
        private static ISet<IntPtr> decryptedCode;

        [DllImport("kernel32.dll")]
        private static extern bool VirtualProtect(IntPtr lpAddress, uint dwSize, uint flNewProtect,
            out uint lpflOldProtect);

        [DllImport("kernel32.dll")]
        private static extern IntPtr LoadLibrary(string lib);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetProcAddress(IntPtr lib, string proc);

        public static void InstallHook()
        {
            decryptedCode = new HashSet<IntPtr>();
            var module = typeof (JitMethodDecryptor).Module;
            moduleRuntimeHandle =
                (IntPtr)
                    module.GetType()
                        .GetField("m_pData", BindingFlags.NonPublic | BindingFlags.Instance)
                        .GetValue(module);

            var clrRuntimeName = Environment.Version.Major == 4 ? "clrjit.dll" : "mscorjit.dll";
            var runtime = LoadLibrary(clrRuntimeName);
            var getJit =
                (GetJit) Marshal.GetDelegateForFunctionPointer(GetProcAddress(runtime, "getJit"), typeof (GetJit));
            var pVTable = getJit();
            var pCompileMethod = Marshal.ReadIntPtr(pVTable);

            // To avoid GC collecting those delegates there are static references to them
            originalCompileMethod = Marshal.GetDelegateForFunctionPointer(Marshal.ReadIntPtr(pCompileMethod),
                typeof (CompileMethodDel));
            hookedDelegate = (CompileMethodDel) HookedCompileMethod;

            RuntimeHelpers.PrepareDelegate(hookedDelegate);
            RuntimeHelpers.PrepareDelegate(originalCompileMethod);

            uint old = 0x40;
            VirtualProtect(pCompileMethod, (uint) IntPtr.Size, old, out old);
            Marshal.WriteIntPtr(pCompileMethod, Marshal.GetFunctionPointerForDelegate(hookedDelegate));
            VirtualProtect(pCompileMethod, (uint) IntPtr.Size, old, out old);
#if RUNTIME_VERBOSE
            Console.WriteLine("[*][JitMethodDecryptor] Hooked at: {0:X} running on: {1} module handle: {2:X}", pCompileMethod, clrRuntimeName, moduleRuntimeHandle);
#endif
        }

        private static int HookedCompileMethod(IntPtr thisPtr, [In] IntPtr corJitInfo,
            [In] IntPtr methodInfo, uint flags, [Out] IntPtr nativeEntry, [Out] IntPtr nativeSizeOfCode)
        {
            var corMethodInfo = (CorMethodInfo*) methodInfo;
            if (moduleRuntimeHandle == corMethodInfo->moduleHandle && !decryptedCode.Contains(corMethodInfo->ilCode))
            {
#if RUNTIME_VERBOSE
                try
                {
                    var token = 0x06000000 + *(ushort*) corMethodInfo->methodHandle;
                    var method = typeof (JitMethodDecryptor).Module.ResolveMethod(token);
                    Console.WriteLine("[*][JitMethodDecryptor] Decrypting method : {0} code: {1:X}",
                        method.DeclaringType == null ? "" : method.DeclaringType.Name + ":" + method.Name,
                        corMethodInfo->ilCode);
                }
                catch (ArgumentOutOfRangeException e)
                {
                    Console.WriteLine("[*][JitMethodDecryptor] Decrypting unknown method code: {0:X}", corMethodInfo->ilCode);
                }
#endif
                uint old = 0x40;
                VirtualProtect(corMethodInfo->ilCode, corMethodInfo->ilCodeSize, old, out old);
                var decryptionKeyBytes = BitConverter.GetBytes(Helper.key);
                var code = (byte*) corMethodInfo->ilCode;
                for (var i = 0; i < corMethodInfo->ilCodeSize; i++)
                {
                    code[i] ^= decryptionKeyBytes[i%decryptionKeyBytes.Length];
                }
                decryptedCode.Add(corMethodInfo->ilCode);
                VirtualProtect(corMethodInfo->ilCode, corMethodInfo->ilCodeSize, old, out old);
            }
            return ((CompileMethodDel) originalCompileMethod)(thisPtr, corJitInfo, methodInfo, flags, nativeEntry,
                nativeSizeOfCode);
        }

        #region JIT_internals

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x88)]
        public struct CorMethodInfo
        {
            public IntPtr methodHandle;
            public IntPtr moduleHandle;
            public IntPtr ilCode;
            public UInt32 ilCodeSize;
            public UInt16 maxStack;
            public UInt16 EHCount;
            public UInt32 corInfoOptions;
        }

        [UnmanagedFunctionPointer(CallingConvention.StdCall, SetLastError = true)]
        private delegate IntPtr GetJit();

        [UnmanagedFunctionPointer(CallingConvention.StdCall, SetLastError = true)]
        public delegate int CompileMethodDel(
            IntPtr thisPtr, [In] IntPtr corJitInfo, [In] IntPtr methodInfo, uint flags, [Out] IntPtr nativeEntry,
            [Out] IntPtr nativeSizeOfCode);

        #endregion JIT_internals
    }
}