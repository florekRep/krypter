﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Runtime
{
    public static class MethodRefProxy
    {
        public static void CreateDelegate(int fieldToken, int methodToken, bool isVirt)
        {
            var moduleHandle = typeof (MethodRefProxy).Module.ModuleHandle;
            var decodedMethodToken = (int) (methodToken ^ Helper.key);
            var decodedFieldToken = (int) (fieldToken ^ Helper.key);
            var fieldInfo = FieldInfo.GetFieldFromHandle(moduleHandle.ResolveFieldHandle(decodedFieldToken));
            var method = (MethodInfo) fieldInfo.Module.ResolveMethod(decodedMethodToken);
#if RUNTIME_VERBOSE
            Console.WriteLine("[*][MethodRefProxy] Resolving method: {0} token: {1:X} proxy: {2} token: {3:X}", method.DeclaringType.Name + ":" + method.Name, decodedMethodToken, fieldInfo.Name, decodedFieldToken);
#endif
            Delegate resultDelegate;
            if (method.IsStatic)
            {
                resultDelegate = Delegate.CreateDelegate(fieldInfo.FieldType, method);
            }
            else
            {
                var parameters = method.GetParameters();
                var paramTypes = new Type[parameters.Length + 1];
                paramTypes[0] = typeof (object);
                for (var i = 1; i < parameters.Length + 1; i++)
                    paramTypes[i] = parameters[i - 1].ParameterType;

                var declaringType = method.DeclaringType;
                var dynamicMethod = new DynamicMethod("", method.ReturnType, paramTypes,
                    (declaringType.IsInterface || declaringType.IsArray) ? fieldInfo.FieldType : declaringType, true);
                var ilGenerator = dynamicMethod.GetILGenerator();
                for (var i = 0; i < paramTypes.Length; i++)
                {
                    ilGenerator.Emit(OpCodes.Ldarg_S, i);
                }
                ilGenerator.Emit(isVirt ? OpCodes.Callvirt : OpCodes.Call, method);
                ilGenerator.Emit(OpCodes.Ret);
                resultDelegate = dynamicMethod.CreateDelegate(fieldInfo.FieldType);
            }
            fieldInfo.SetValue(null, resultDelegate);
        }
    }
}